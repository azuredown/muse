TODO: Define scratchspace

# Types

TODO

# Logical Operators (Unary, Binary, Etc)

TODO

# Logical Statements

TODO

# Containers

A container is a basic building block of logic in Muse. A container can act as a 'function':

```
foo(int x) : int y:
	pass
```

A container can also be a 'class':

```
foo1(int x):
	foo(int x) -> int y:
		pass
```

But a container cannot be a 'namespace' (this may or may not be implemented later).

More formally a "container" is a list of instructions to run which may contain a sublist of instructions. A container with no arguments can be called like so:

```
- foo
```

or alternately with brackets:

```
- (foo)
```

Similarly you can call functions that take arguments with or without brackets. But if the meaning is ambiguous or the container call is nested in another container call brackets are required. (I have a feeling I'm going to regret this decision)

```
- (foo x)
- foo1 (foo x)
```

and if it gives a return value it can be (optionally) assigned.

```
- x <- foo
```

Calling a container executes each instruction it contains sequentially.

In addition to this a container can be 'saved' using the new keyword.

```
- x <- new (foo)
```

(Note that if the container previously returned something the return value is lost.)

This means that in addition to executing every instruction not in subcontainers the container itself will be saved to a variable (in this case `x`) instead of being destroyed. It is then possible to get or set any variable in the container's scratchspace or call additional subcontainers. Or even assign subcontainers to variables.

## Accessing Information (Variables/Methods) In Parent/Child Containers

Some languages allow you to access a variable belonging to the parent class ('global variables') by simply calling them as if they were local variables. This is... this is a bad idea, OK.

In Muse you must reference variables using a text string that indicates the relative position of the variable we wish to access (the "address"). We do this using unix directory notation. So to access a variable belonging to the parent container (a 'global' variable) you use `../VarYouWant`. To access a variable in a child container you use `ChildContainer/ChildVar`. Note that she single dot (in unix this is used to refer to the current directory) is not supported.

Although this could hypothetically be used to access variable belonging to containers at any depth, we restrict this to only accessing one container up or down for now. And no crazy loops like `../ThisContainer/../ThisContainer/../someVar` either.

## Container Header

Each container begins with a single line indicating the container's name, return type (if not null), and any arguments it takes (the "header").

Here is an example of a container header with everything:

```
foo(int x, int y) -> int z:
	pass
```

Here is an example without arguments:

```
foo() -> int x:
	pass
```

And here is an example with no return value.

```
foo(int x, int y):
	pass
```

Note that unlike other languages the return value's name must be explicitly stated.


## Variables In Other Containers

TODO


## Optional And Named Arguments

TODO


## Maximum Container Depth

A container can only hold one level of subcontainer (similar to a class and function in a other object oriented languages). Any more should display a warning/error. An error if this compiler does not support this feature or a warning otherwise.


# Annotations

Just like C# Muse has Annotations. These are a set of instructions on how a container (and optionally all containers under it) should behave (the "annotations").

There are three types of annotations: annotations that change the compiler behaviour (such as indicating a particular container is 'private'), annotations that modify how log messages are displayed (suppressing warnings), and annotations that change functionality of the container (defaulting to a value type).

To create an annotation simply place them before the container they act on but after any previous container. For example:

```
[Example container 1]
[Example container 2]

[Example container 3]
foo <- container() {
}
```

Containers must not be on the same line as the container header. Multiple annotations may apply to a single container. Annotations may be sepparated by a single white line (but no more) as seen in the example.

Annotations must be surrounded by square brackets as shown in the example.

TODO Add stock annotations

TODO Add a way to script custom annotations


# Types

TODO

## Monads

TODO


# Inherritence

In a typical language a class can inherrit from another class (its parent) and the inherriting class will automagically gain access to all public and protected methods and properties. This may save time in the short term but can be quite annoying while debugging as you will need to jump between multiple class definitions to find the methods being called.



# Interfaces And Casting



# Commenting And Documentation

For single line comments we use `//` and for multiline comments we use `/* */`.

The single line comment works how you'd expect. Everything on the line after these characters are for documentation purposes only (unless when explicitly stated otherwise) and do not effect the rest of the program.

Multiline comments work a bit differently, however. Similar to how braces work we must first identify the outer `/*` and `*/` symbols. More specifically a 'commented zone' is defined between every pair of `/* */` symbols except for if the symbols themselves lie in a commented zone. The runtime must maximize the amount of code inside commented zones.

Similar to `//` comments the text in the commented zones is for documentation unless otherwise stated and does not effect the rest of the program.

The reason for this additional consideration is if a block of code is commented with a multi-line comment that itself contains a multi-line comment. This can be quite frustrating as other languages multi-line comments are fairly dumb and take a greedy approach.

One additional requirement that we must consider for `/*` and `*/` are escape characters. The escaped versions of these are `\/*` and `\*/`. As you can see there is only one backslash. No need to go crazy.

Also this allows us to be stricter in the use of these characters. So here we isist hat if there is a single (non-escaped) `/*` or `*/` inside the commented zone then this is a compiler warning.


# Escape Characters


# Prototypes and Double Pointers

Every single object has a 'prototype' reference. This is the reference the object was created with.


This reference controls if this variable should be garbage collected or not (when the prototype reference or the object holding the prototype reference become null or no longer point to the prototype the object is marked for garbage collection). To define one you must use:

```
prot int x <- 1
```

or I guess

```
prot int x
```

Passing a prototype reference to another object is not currently supported. However you can make a copy of an object and assign that to a new prototype reference like this:

```
prot int xClone <- &x
```

The reason we use the `&` symbol is because we're not getting a pointer to the object (as we'd normally do), but the object itself. This is the case for if operating on the prototype reference directly. If operating on a pointer to a reference object (a double pointer) we'd have to do:

```
prot int xClone <- &&xPointer
```

## Double Pointers

The purpose of double pointers is so that if we assign a value to the double pointer the original pointer gets it's value changed as well. This allows you to do things like:

```
foo(int x):
	x <- 2

var <- 1
foo var
print var
```

Where value printed is '2' rather than '1' in some other languages. Note that this is not in any way like C's double pointer.

So in summary every object has a single prototype reference (single pointer) and then any number of pointer to pointers (double pointers). Managing of pointers is done automatically for you (you do not have to use and `*` or `&` symbols in your code).





