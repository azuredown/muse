# Intro

Muse extends the robust properties feature of C# and makes it truly integrated into the language. A programmer should not have to worry if something is a property or a variable. So in Muse the 2 are one in the same.

It may be required for something to absolutely be a variable, such as during serialization. These cases will be implemented on a case by case basis.

# Global and local variables

All code is kept in methods and functions. There is no code haphazardly thrown together in methods. Instead variables must be declared in the constructor. So then, one may wonder, what happens to global variables.

And the answer is variables follow a unix directory style. Picture the class as a unix file system with the following format:

```
Constructor
	Method1
	Method2
	Method3
```

If we declared the global variable in the constructor using the syntax `./foo` we could access it in a method using the syntax `../foo`. Similarly from any method we can declare a variable global and *persistent* to the constructor using `../foo`.

If we declared the variable from *method1* using `./foo` we would not be able to access it from any other method (although we can still access it from method1 and it is still global) because this feature isn’t implemented yet. We will eventually be able to access it by doing `../method1/foo`.

This is due to often having variables I’d only want to call from one method. This could be something such as time (full disclosure: this is the only example I can think of at the moment). I want to keep track of the time variable across frames but there is no need to expose the variable to every other method in my class.

Local variables, on the other hand, are just declared the normal way. Namely `foo`.

# Naming conventions

Variable, method, and label names in Muse must be suitably distinct. This means names can not differ by only case. So a variable can not have the same name as a method.

This is based on a bug I once encountered where the I was using the variable *tileId* when I should have been using the variable *tileID*.

# Syntax

The following syntax is used for variables:

```
type x <- y // When called x returns value y
type x -> y <> value // When assigning the value value to x value is assigned to x
type x <> type // Not yet implemented. X both gets and sets the value of a compiler-managed variable.
```

Eventually we may eliminate the initial type keyword and just infer the type based on what we’re assigning to but this is not implemented yet.

Some *purists* may find the use of the arrow inappropriate as reversing the line: `y <- x` instead of `x -> y` does not produce the same result. It may not be perfect but if we keep in mind the value on the left is what we write in an actual statement it makes perfect sense why this is.

One thing we wish to avoid is having redundant variables. Eventually we will be able to call `type` in our method setter anywhere to refer to the compiler-managed variable. In C# we could only do this if we had an auto-property.

# Why not get rid of setters?

Although technically possible, removing setters would force every single class to be fully encapsulated. This may sound nice but will put an undo burden on the developer, especially during debugging which will involve the *ping pong problem* where classes call each other back and forth for no good reason.