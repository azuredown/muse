# Muse - A Dysfunctional Language

Muse is an implicitly typed object oriented language that compiles to JVM bytecode.

## Philosophy

![Where We're Going We Don't Need No Functions](Images/Functions.png)

### Some Backstory

The original reason I wanted to create a language was to simplify all these concepts you see in other COOLs (C-like Object Oriented Languages). Things like private and public and classes and functions and namespaces. My goal was to make the compiler smart enough so that you didn't need to do as much typing.

But around this time I was also hearing about these snobbish functional programmers. And eventually I was like, "Oh, yeah? What if I made a language without functions? A dysfunctional language." And then I was like, "Woah, dude."

### A Dysfunctional Language

So I made Muse a 'Dysfunctional' Language. Meaning that every function could also be a class. This later became watered down to being only void functions could be classes. You can read the readme for it [here](https://gitlab.com/azuredown/muse/-/tree/6d1d96e2611549bc74f3bef04f5c4049406ba9d8).

But then I realized that doing this means having a lot of duplicate code in my JVM class files. It's not very efficient. And there was a lot of confusion on what a 'Dysfunctional' language even meant. So then, I thought, what if we just go all in on this dysfunctional idea?

### Enter Muse 2.0: 99.99% Less Functional Than Muse 1.0

And that is what I call Muse 2.0. Full dysfunctional as in there are no functions (except for the first 'main' function, technically). So then how do we do things in a fully dysfunctional language? Well, it's simple. We can emulate functions by creating a class, getting a variable (like result or something), and then immediately destroying the class.

And your first thought might be, "Isn't that a bit inefficient?" And, yeah, a bit. I actually did a bit of experimentation once and allocating memory is surprisingly expensive even before applying garbage collection. Of course there are some fine zero overhead and low overhead garbage collection methods out there. Originally I planned for objects in Muse to only have one reference so they could easily be destroyed and you can access objects by just traversing the tree of objects.

But anyways I'm getting ahead of myself. Creating objects on the heap appears to take at least 10x as long as creating them on the stack. This is really hard to stomach. But then again...

![You Gotta Commit To The Gimmick](Images/CommitToTheGimmick.gif)

Yeah, I mean, not everything we do in programming is done for speed. Look at Python. It's interpreted, but people still use it. And perhaps in the future we can add an optimization to Muse that makes it instantiate new dysfunctions (objects only instantiated for their constructor) on the stack rather than the heap thereby eliminating most of the overhead.

## Building

This project uses ANTLR. You can read the ANTLR documentation [here](https://github.com/antlr/antlr4/blob/master/doc/index.md).

Or if you just want to run the code go to the [getting started](https://github.com/antlr/antlr4/blob/master/doc/getting-started.md) section and follow the instructions there.

### Quickstart

To get started simply compile all the Java files in the `src/` directory. Then you can run `MuseMain` (eg: `java MuseMain SOURCE_FILE.muse`) to compile and run a muse file. Or use the `-c` flag to only compile and not run.

Alternately you can also run `tests/RunTests.py` (covered below) to automate the entire process.

### Building And Testing Grammar

If you make a change to the grammar (in the `src/Muse.g4` file) then you will need to recompile the grammar. To do so run the commands:

```
antlr4 Muse.g4
javac Muse*.java
```

Testing of the grammar can then be done by using `grun GRAMMAR_NAME RULE_NAME -tree` to view the tree in STDOUT or or `grun GRAMMAR_NAME RULE_NAME -gui` to view the tree in a gui. Once one of these commands are run enter the string to parse (press enter for new lines) and then use Ctrl+Z (Windows) or Ctrl+D (Unix) followed by enter to parse.

Although a much easier way to do this is to use the IntelliJ ANTLR plugin. This will allow you to see changes in the syntax tree in real time.

### Windows Error

If you have this error:

```
> javac Hello*.java
HelloBaseListener.java:3: package org.antlr.v4.runtime does not exist
import org.antlr.v4.runtime.ParserRuleContext;
```

as detailed [here](https://github.com/antlr/antlr4/blob/master/doc/faq/installation.md) make sure that the complete ANTLR file is in your CLASSPATH as .;C:\Javalib\antlr-4.8-complete.jar;%CLASSPATH%. Where C:\Javalib\antlr-4.8-complete.jar is the version of ANTLR I'm using.

## Debugging

You can view the JVM bytecode by typing in `javap -verbose MuseMain` where MuseMain is the name of the class file we're viewing. This will output the contents of the java class file in a surprisingly human readable format.

Similarly there are also the commands `javap -c MuseMain` which only prints out the code (assembly) portion.

You can also use a hex editor to view the class files for additional details. If you do this you will need the [JVM .class file specification](https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html#jvms-4.7.4) to tell what everything is.

## Tests

There are tests in the tests directory. To run them simply run the file `RunTests.py` on a unix style terminal (such as Git Bash, MacOS, or Linux) with python installed. This will compile all the .muse files and assert that their output is correct.

## JVM Notes

The JVM does not use registers. Instead it uses a stack of values and an array of variables.

## Restrictions

- Int values must not end with ++/--
- Variables must not contain > < = >= <= + - * / ' and ' ' or ' : % ) (
- Variables cannot start with ../
- Class names cannot contain /
- Functions must not be called <init> or <clinit>
- Class names may not begin with $

## Features

- You can start a variable name with a number. I always wanted to do this.
- Goto statements

![Fuck yeah](https://thumbs.gfycat.com/HelpfulPhonyBluebreastedkookaburra-small.gif)

- No constructors. All the arguments you pass to a class are automatically set as it's 'global' variables. So no more `this.var = var`. Dart actually has an interesting way of putting arguments in curly braces to do something similar but I decided to go all the way.


## Todo

- Allow tagging of dysfunctions (pure and threadsafe)
- Allow yield to create IEnumerators
- Multiply string/chars by int to multiply them
- Variables that start with a _ are automatically private like Dart
- When accessing a variable in a parent container, a 'global' variable, you have to use unix directory syntax like ../var1. Because why oh why do modern languages use the same syntax to access global and local variables.
- Add way to explicitly declare types. Right now it's implicit (the same as C\#'s `var`) but we may want an option to explicitly define them such as `int x <- 3`
- Write parser with better support for comments
- Jumping
	- Arbitrary number of added variables
- Late (inspired by Dart) (once var is assigned it cannot be assigned to null)/Null
- The '..' syntax used in some modern languages
- Disable use of reserved names
- 32 and 64 bit support
	- In int assignments
	- In *= += -= /=
- Make assignments work with strings and variables
- Add support for over 4 variables
- Tail recursion
- Make print statements work with multiple arguments
- Optional arguments
- Named arguments
- Extension methods
- Tuples
- Write the Muse compiler in Muse
- Ability to inline functions/methods
- ?Add LineNumberTable attribute for debugging
- Figure out what's the last attribute that's added to every .class file
- Dynamic int sizes so we can never overflow
- Add check if full stack size is not used
- Threading
    - Threaded classes must be marked as either non-main only or threadable
- Code obfuscation
- Consistent division (always returns floating point)
- Ability to pass an arbitrary number of arguments like params[] in Java/C#
- Get strong typing for object types, not just whether something is an object or not

### Error messages

- Readd syntax error for when variable does not exist

## Possible Optimizations (That Java does)

- If comparing to zero don't have to load an extra value into the stack
- Infinite loops don't need a return
- Create constants
