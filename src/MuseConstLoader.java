import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MuseConstLoader implements MuseListener {

	private ClassFileSharedData sharedData;

	public MuseConstLoader(ClassFileSharedData sharedData) {
		this.sharedData = sharedData;
	}

	private Stack<List<String>> stackOfListsOfTokens = new Stack<List<String>>();

	public void enterProgram(MuseParser.ProgramContext ctx) { }

	public void exitProgram(MuseParser.ProgramContext ctx) { }

	public void enterDysfunction_op(MuseParser.Dysfunction_opContext ctx) { }

	public void exitDysfunction_op(MuseParser.Dysfunction_opContext ctx) { }

	public void enterLambda_op(MuseParser.Lambda_opContext ctx) { }

	public void exitLambda_op(MuseParser.Lambda_opContext ctx) {

		String command = stackOfListsOfTokens.peek().get(0);
		String name = command.substring(command.indexOf("const") + 5, command.indexOf("<-"));
		name = MuseUtils.trim(name);

		String parameters = MuseUtils.trim(command.substring(command.lastIndexOf("lambda") + 6));
		if (parameters.endsWith(":")) {
			parameters = MuseUtils.trim(parameters.substring(0, parameters.length() - 1));
		}

		List<Pair<VariableType, String>> parameterList = new ArrayList<Pair<VariableType, String>>();

		while (parameters.startsWith("int")) {
			parameters = MuseUtils.trim(parameters.substring(parameters.indexOf("int") + 3));
			if (parameters.contains(" ")) {
				parameterList.add(new Pair<VariableType, String>(VariableType.Int, parameters.substring(0, parameters.indexOf(" "))));
				parameters = MuseUtils.trim(parameters.substring(parameters.indexOf(" ") + 1));
			} else {
				parameters = MuseUtils.trim(parameters.substring(parameters.indexOf(" ") + 1));
				parameterList.add(new Pair<VariableType, String>(VariableType.Int, parameters));
			}
		}

		ContainerVariableInfo info = new ContainerVariableInfo(null, parameterList);
		sharedData.containerSignatures.put(name, info);

		ConstantPoolManager.createMethodReferenceConstant(sharedData, name, info.makeTypeSignature());
	}

	public void enterAssignment_op(MuseParser.Assignment_opContext ctx) { }

	public void exitAssignment_op(MuseParser.Assignment_opContext ctx) { }

	public void enterPrint_op(MuseParser.Print_opContext ctx) { }

	public void exitPrint_op(MuseParser.Print_opContext ctx) { }

	public void enterIf_op(MuseParser.If_opContext ctx) { }

	public void exitIf_op(MuseParser.If_opContext ctx) { }

	public void enterElif_op(MuseParser.Elif_opContext ctx) { }

	public void exitElif_op(MuseParser.Elif_opContext ctx) { }

	public void enterElse_op(MuseParser.Else_opContext ctx) { }

	public void exitElse_op(MuseParser.Else_opContext ctx) { }

	public void enterLabel_op(MuseParser.Label_opContext ctx) { }

	public void exitLabel_op(MuseParser.Label_opContext ctx) { }

	public void enterGoto_op(MuseParser.Goto_opContext ctx) { }

	public void exitGoto_op(MuseParser.Goto_opContext ctx) { }

	public void enterIncrementDecrement_op(MuseParser.IncrementDecrement_opContext ctx) { }

	public void exitIncrementDecrement_op(MuseParser.IncrementDecrement_opContext ctx) { }

	public void enterOperationEquals_op(MuseParser.OperationEquals_opContext ctx) { }

	public void exitOperationEquals_op(MuseParser.OperationEquals_opContext ctx) { }

	public void enterLoop_op(MuseParser.Loop_opContext ctx) { }

	public void exitLoop_op(MuseParser.Loop_opContext ctx) { }

	public void enterBreak_op(MuseParser.Break_opContext ctx) {	}

	public void exitBreak_op(MuseParser.Break_opContext ctx) {}

	public void enterEveryRule(ParserRuleContext ctx) {
		stackOfListsOfTokens.push(new ArrayList<String>());
	}

	public void exitEveryRule(ParserRuleContext ctx) {
		stackOfListsOfTokens.pop();
	}

	public void visitTerminal(TerminalNode node) {
		stackOfListsOfTokens.get(stackOfListsOfTokens.size() - 1).add(node.toString());
	}

	public void visitErrorNode(ErrorNode node) {
		throw new RuntimeException("Syntax error when trying to visit node: " + node.toString());
	}
}