import java.util.List;

public class MyLinkedList<T> {
	public LinkedListNode<T> first = null;
	public LinkedListNode<T> last = null;

	public int size = 0;

	// For debugging
	public int recomputeSize() {
		LinkedListNode<T> node = first;
		int newCount = 0;
		while (node != null && !node.temporaryNode) {
			newCount++;
			node = node.next;
		}
		return newCount;
	}

	public void append(List<T> other) {
		for (int i = 0; i < other.size(); i++) {
			add(other.get(i));
		}
	}

	public void append(T[] other) {
		for (int i = 0; i < other.length; i++) {
			add(other[i]);
		}
	}

	public void append(MyLinkedList<T> other) {

		if (other.last == null) {
			return;
		} else if (other.last.temporaryNode) {
			throw new RuntimeException("Appending linked list has a temporary node.");
		}

		if (last != null && last.temporaryNode) {
			last.temporaryNode = false;
			last.item = other.first.item;
			size++;
			other.removeFirst();
		}

		if (other.last == null) {
			return;
		}

		if (last != null) {
			last.next = other.first;
		} else {
			first = other.first;
		}
		last = other.last;
		size += other.size;
	}

	/*
	public void removeLastNode() {
		LinkedListNode<T> previous = null;
		LinkedListNode<T> node = first;
		while (node != null && !node.temporaryNode) {
			previous = node;
			node = node.next;
		}
		last = previous;
		size--;
	}
	*/

	public void removeFirst() {
		first = first.next;
		if (first == null) {
			last = null;
		}
		size--;
	}

	public LinkedListNode<T> getAt(int index) {
		int counter = 0;
		LinkedListNode<T> toReturn = first;

		while (counter < index) {
			counter++;
			toReturn = toReturn.next;
		}

		return toReturn;
	}

	public int getIndex(LinkedListNode<T> node){
		int counter = 0;
		LinkedListNode<T> currentNode = first;
		while (currentNode != node) {
			currentNode = currentNode.next;
			counter++;
			if (currentNode == null) {
				throw new RuntimeException("Unable to find node " + node + "!");
			}
		}
		return counter;
	}

	public void add(T toAdd) {
		size++;

		if (last == null) {
			LinkedListNode<T> itemToAdd = new LinkedListNode<T>(toAdd);
			first = itemToAdd;
			last = itemToAdd;
		} else if (last.temporaryNode) {
			last.temporaryNode = false;
			last.item = toAdd;
		} else {
			LinkedListNode<T> itemToAdd = new LinkedListNode<T>(toAdd);
			last.next = itemToAdd;
			last = itemToAdd;
		}
	}


	public void addEmptyNode() {

		if (last == null) {
			LinkedListNode<T> itemToAdd = new LinkedListNode<T>();
			itemToAdd.temporaryNode = true;
			first = itemToAdd;
			last = itemToAdd;
		// Already added a temporary node
		} else if (last.temporaryNode) {
		} else {
			LinkedListNode<T> itemToAdd = new LinkedListNode<T>();
			itemToAdd.temporaryNode = true;
			last.next = itemToAdd;
			last = itemToAdd;
		}
	}
}