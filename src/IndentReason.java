public enum IndentReason {
	If,
	While,
	DoWhile,
	Else,
	Elif,
}