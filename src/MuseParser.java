// Generated from Muse.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MuseParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT_MULTILINE=1, COMMENT_SINGLELINE=2, BREAK=3, OPERATION_EQUALS=4, 
		LABEL=5, GOTO=6, IF=7, ELIF=8, ELSE=9, LOOP=10, CONST=11, LAMBDA=12, ASSIGNMENT=13, 
		BOOL=14, INCREMENT_DECREMENT=15, STRING=16, INTEGER=17, PRINT=18, DYSFUNCTION=19, 
		DANGLING_INDENTS=20, EXPRESSION=21, EXPRESSION2=22, LEADING_TABS=23, WHITESPACE=24;
	public static final int
		RULE_program = 0, RULE_dysfunction_op = 1, RULE_lambda_op = 2, RULE_assignment_op = 3, 
		RULE_print_op = 4, RULE_if_op = 5, RULE_elif_op = 6, RULE_else_op = 7, 
		RULE_label_op = 8, RULE_goto_op = 9, RULE_incrementDecrement_op = 10, 
		RULE_operationEquals_op = 11, RULE_loop_op = 12, RULE_break_op = 13;
	private static String[] makeRuleNames() {
		return new String[] {
			"program", "dysfunction_op", "lambda_op", "assignment_op", "print_op", 
			"if_op", "elif_op", "else_op", "label_op", "goto_op", "incrementDecrement_op", 
			"operationEquals_op", "loop_op", "break_op"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, "'const'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "COMMENT_MULTILINE", "COMMENT_SINGLELINE", "BREAK", "OPERATION_EQUALS", 
			"LABEL", "GOTO", "IF", "ELIF", "ELSE", "LOOP", "CONST", "LAMBDA", "ASSIGNMENT", 
			"BOOL", "INCREMENT_DECREMENT", "STRING", "INTEGER", "PRINT", "DYSFUNCTION", 
			"DANGLING_INDENTS", "EXPRESSION", "EXPRESSION2", "LEADING_TABS", "WHITESPACE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Muse.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MuseParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(MuseParser.EOF, 0); }
		public List<Assignment_opContext> assignment_op() {
			return getRuleContexts(Assignment_opContext.class);
		}
		public Assignment_opContext assignment_op(int i) {
			return getRuleContext(Assignment_opContext.class,i);
		}
		public List<Goto_opContext> goto_op() {
			return getRuleContexts(Goto_opContext.class);
		}
		public Goto_opContext goto_op(int i) {
			return getRuleContext(Goto_opContext.class,i);
		}
		public List<Print_opContext> print_op() {
			return getRuleContexts(Print_opContext.class);
		}
		public Print_opContext print_op(int i) {
			return getRuleContext(Print_opContext.class,i);
		}
		public List<Label_opContext> label_op() {
			return getRuleContexts(Label_opContext.class);
		}
		public Label_opContext label_op(int i) {
			return getRuleContext(Label_opContext.class,i);
		}
		public List<IncrementDecrement_opContext> incrementDecrement_op() {
			return getRuleContexts(IncrementDecrement_opContext.class);
		}
		public IncrementDecrement_opContext incrementDecrement_op(int i) {
			return getRuleContext(IncrementDecrement_opContext.class,i);
		}
		public List<OperationEquals_opContext> operationEquals_op() {
			return getRuleContexts(OperationEquals_opContext.class);
		}
		public OperationEquals_opContext operationEquals_op(int i) {
			return getRuleContext(OperationEquals_opContext.class,i);
		}
		public List<If_opContext> if_op() {
			return getRuleContexts(If_opContext.class);
		}
		public If_opContext if_op(int i) {
			return getRuleContext(If_opContext.class,i);
		}
		public List<Loop_opContext> loop_op() {
			return getRuleContexts(Loop_opContext.class);
		}
		public Loop_opContext loop_op(int i) {
			return getRuleContext(Loop_opContext.class,i);
		}
		public List<Lambda_opContext> lambda_op() {
			return getRuleContexts(Lambda_opContext.class);
		}
		public Lambda_opContext lambda_op(int i) {
			return getRuleContext(Lambda_opContext.class,i);
		}
		public List<Dysfunction_opContext> dysfunction_op() {
			return getRuleContexts(Dysfunction_opContext.class);
		}
		public Dysfunction_opContext dysfunction_op(int i) {
			return getRuleContext(Dysfunction_opContext.class,i);
		}
		public List<Break_opContext> break_op() {
			return getRuleContexts(Break_opContext.class);
		}
		public Break_opContext break_op(int i) {
			return getRuleContext(Break_opContext.class,i);
		}
		public List<Else_opContext> else_op() {
			return getRuleContexts(Else_opContext.class);
		}
		public Else_opContext else_op(int i) {
			return getRuleContext(Else_opContext.class,i);
		}
		public List<Elif_opContext> elif_op() {
			return getRuleContexts(Elif_opContext.class);
		}
		public Elif_opContext elif_op(int i) {
			return getRuleContext(Elif_opContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BREAK) | (1L << OPERATION_EQUALS) | (1L << LABEL) | (1L << GOTO) | (1L << IF) | (1L << ELIF) | (1L << ELSE) | (1L << LOOP) | (1L << LAMBDA) | (1L << ASSIGNMENT) | (1L << INCREMENT_DECREMENT) | (1L << PRINT) | (1L << DYSFUNCTION))) != 0)) {
				{
				setState(41);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ASSIGNMENT:
					{
					setState(28);
					assignment_op();
					}
					break;
				case GOTO:
					{
					setState(29);
					goto_op();
					}
					break;
				case PRINT:
					{
					setState(30);
					print_op();
					}
					break;
				case LABEL:
					{
					setState(31);
					label_op();
					}
					break;
				case INCREMENT_DECREMENT:
					{
					setState(32);
					incrementDecrement_op();
					}
					break;
				case OPERATION_EQUALS:
					{
					setState(33);
					operationEquals_op();
					}
					break;
				case IF:
					{
					setState(34);
					if_op();
					}
					break;
				case LOOP:
					{
					setState(35);
					loop_op();
					}
					break;
				case LAMBDA:
					{
					setState(36);
					lambda_op();
					}
					break;
				case DYSFUNCTION:
					{
					setState(37);
					dysfunction_op();
					}
					break;
				case BREAK:
					{
					setState(38);
					break_op();
					}
					break;
				case ELSE:
					{
					setState(39);
					else_op();
					}
					break;
				case ELIF:
					{
					setState(40);
					elif_op();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(45);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(46);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dysfunction_opContext extends ParserRuleContext {
		public TerminalNode DYSFUNCTION() { return getToken(MuseParser.DYSFUNCTION, 0); }
		public Dysfunction_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dysfunction_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterDysfunction_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitDysfunction_op(this);
		}
	}

	public final Dysfunction_opContext dysfunction_op() throws RecognitionException {
		Dysfunction_opContext _localctx = new Dysfunction_opContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_dysfunction_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48);
			match(DYSFUNCTION);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lambda_opContext extends ParserRuleContext {
		public TerminalNode LAMBDA() { return getToken(MuseParser.LAMBDA, 0); }
		public Lambda_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lambda_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterLambda_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitLambda_op(this);
		}
	}

	public final Lambda_opContext lambda_op() throws RecognitionException {
		Lambda_opContext _localctx = new Lambda_opContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_lambda_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50);
			match(LAMBDA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_opContext extends ParserRuleContext {
		public TerminalNode ASSIGNMENT() { return getToken(MuseParser.ASSIGNMENT, 0); }
		public Assignment_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterAssignment_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitAssignment_op(this);
		}
	}

	public final Assignment_opContext assignment_op() throws RecognitionException {
		Assignment_opContext _localctx = new Assignment_opContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_assignment_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(52);
			match(ASSIGNMENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Print_opContext extends ParserRuleContext {
		public TerminalNode PRINT() { return getToken(MuseParser.PRINT, 0); }
		public Print_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterPrint_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitPrint_op(this);
		}
	}

	public final Print_opContext print_op() throws RecognitionException {
		Print_opContext _localctx = new Print_opContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_print_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			match(PRINT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_opContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(MuseParser.IF, 0); }
		public If_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterIf_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitIf_op(this);
		}
	}

	public final If_opContext if_op() throws RecognitionException {
		If_opContext _localctx = new If_opContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_if_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			match(IF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Elif_opContext extends ParserRuleContext {
		public TerminalNode ELIF() { return getToken(MuseParser.ELIF, 0); }
		public Elif_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elif_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterElif_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitElif_op(this);
		}
	}

	public final Elif_opContext elif_op() throws RecognitionException {
		Elif_opContext _localctx = new Elif_opContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_elif_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			match(ELIF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_opContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(MuseParser.ELSE, 0); }
		public Else_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterElse_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitElse_op(this);
		}
	}

	public final Else_opContext else_op() throws RecognitionException {
		Else_opContext _localctx = new Else_opContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_else_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			match(ELSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Label_opContext extends ParserRuleContext {
		public TerminalNode LABEL() { return getToken(MuseParser.LABEL, 0); }
		public Label_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_label_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterLabel_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitLabel_op(this);
		}
	}

	public final Label_opContext label_op() throws RecognitionException {
		Label_opContext _localctx = new Label_opContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_label_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(62);
			match(LABEL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Goto_opContext extends ParserRuleContext {
		public TerminalNode GOTO() { return getToken(MuseParser.GOTO, 0); }
		public Goto_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_goto_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterGoto_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitGoto_op(this);
		}
	}

	public final Goto_opContext goto_op() throws RecognitionException {
		Goto_opContext _localctx = new Goto_opContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_goto_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(64);
			match(GOTO);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IncrementDecrement_opContext extends ParserRuleContext {
		public TerminalNode INCREMENT_DECREMENT() { return getToken(MuseParser.INCREMENT_DECREMENT, 0); }
		public IncrementDecrement_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_incrementDecrement_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterIncrementDecrement_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitIncrementDecrement_op(this);
		}
	}

	public final IncrementDecrement_opContext incrementDecrement_op() throws RecognitionException {
		IncrementDecrement_opContext _localctx = new IncrementDecrement_opContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_incrementDecrement_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66);
			match(INCREMENT_DECREMENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperationEquals_opContext extends ParserRuleContext {
		public TerminalNode OPERATION_EQUALS() { return getToken(MuseParser.OPERATION_EQUALS, 0); }
		public OperationEquals_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operationEquals_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterOperationEquals_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitOperationEquals_op(this);
		}
	}

	public final OperationEquals_opContext operationEquals_op() throws RecognitionException {
		OperationEquals_opContext _localctx = new OperationEquals_opContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_operationEquals_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68);
			match(OPERATION_EQUALS);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Loop_opContext extends ParserRuleContext {
		public TerminalNode LOOP() { return getToken(MuseParser.LOOP, 0); }
		public Loop_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loop_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterLoop_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitLoop_op(this);
		}
	}

	public final Loop_opContext loop_op() throws RecognitionException {
		Loop_opContext _localctx = new Loop_opContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_loop_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70);
			match(LOOP);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Break_opContext extends ParserRuleContext {
		public TerminalNode BREAK() { return getToken(MuseParser.BREAK, 0); }
		public Break_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_break_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterBreak_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitBreak_op(this);
		}
	}

	public final Break_opContext break_op() throws RecognitionException {
		Break_opContext _localctx = new Break_opContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_break_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72);
			match(BREAK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\32M\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\7\2,\n\2\f\2\16\2/\13\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3"+
		"\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3"+
		"\16\3\16\3\17\3\17\3\17\2\2\20\2\4\6\b\n\f\16\20\22\24\26\30\32\34\2\2"+
		"\2K\2-\3\2\2\2\4\62\3\2\2\2\6\64\3\2\2\2\b\66\3\2\2\2\n8\3\2\2\2\f:\3"+
		"\2\2\2\16<\3\2\2\2\20>\3\2\2\2\22@\3\2\2\2\24B\3\2\2\2\26D\3\2\2\2\30"+
		"F\3\2\2\2\32H\3\2\2\2\34J\3\2\2\2\36,\5\b\5\2\37,\5\24\13\2 ,\5\n\6\2"+
		"!,\5\22\n\2\",\5\26\f\2#,\5\30\r\2$,\5\f\7\2%,\5\32\16\2&,\5\6\4\2\',"+
		"\5\4\3\2(,\5\34\17\2),\5\20\t\2*,\5\16\b\2+\36\3\2\2\2+\37\3\2\2\2+ \3"+
		"\2\2\2+!\3\2\2\2+\"\3\2\2\2+#\3\2\2\2+$\3\2\2\2+%\3\2\2\2+&\3\2\2\2+\'"+
		"\3\2\2\2+(\3\2\2\2+)\3\2\2\2+*\3\2\2\2,/\3\2\2\2-+\3\2\2\2-.\3\2\2\2."+
		"\60\3\2\2\2/-\3\2\2\2\60\61\7\2\2\3\61\3\3\2\2\2\62\63\7\25\2\2\63\5\3"+
		"\2\2\2\64\65\7\16\2\2\65\7\3\2\2\2\66\67\7\17\2\2\67\t\3\2\2\289\7\24"+
		"\2\29\13\3\2\2\2:;\7\t\2\2;\r\3\2\2\2<=\7\n\2\2=\17\3\2\2\2>?\7\13\2\2"+
		"?\21\3\2\2\2@A\7\7\2\2A\23\3\2\2\2BC\7\b\2\2C\25\3\2\2\2DE\7\21\2\2E\27"+
		"\3\2\2\2FG\7\6\2\2G\31\3\2\2\2HI\7\f\2\2I\33\3\2\2\2JK\7\5\2\2K\35\3\2"+
		"\2\2\4+-";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}