import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.misc.Triple;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LoadManager {

	public static boolean tryLoadValue(ClassFileSharedData sharedData, String variableName, MyLinkedList<Byte> codeContent,
									   Map<String, Pair<Integer, VariableType>> variablesInUse,
									   Map<String, Triple<Integer, VariableType, Boolean>> staticVariablesInUse,
									   Map<ConstantType, Map<String, Integer>> constantIndices,
									   ContainerBlock currentContainer) {

		if (variablesInUse.containsKey(variableName)) {
			JVMInstructions.iLoad(codeContent, variablesInUse.get(variableName).a);
			return true;
		}

		while (variableName.startsWith("../")) {
			variableName = variableName.substring(3);
			currentContainer = currentContainer.parentContainer;
		}

		String afterSlash = "";
		String baseClass = "";
		if (variableName.contains("/")) {
			afterSlash = variableName.substring(variableName.indexOf("/") + 1);
			baseClass = variableName.substring(0, variableName.indexOf("/"));
		} else {
			baseClass = variableName;
		}

		if (staticVariablesInUse.containsKey(baseClass)) {

			if (!currentContainer.ourStaticVariables.contains(baseClass)) {
				throw new RuntimeException("Trying to access variable '" + baseClass + "' using the incorrect container '" + currentContainer.name + "'.");
			}

			if (afterSlash.equals("")) {
				JVMInstructions.getStatic(codeContent, constantIndices, MuseUtils.fieldName(sharedData.className, baseClass));
			} else {

				List<Pair<VariableType, String>> parameters = sharedData.containerSignatures.get(sharedData.objectTypes.get(baseClass)).parameters;
				VariableType typeInfo = null;

				for (int i = 0; i < parameters.size(); i++) {
					if (parameters.get(i).b.equals(afterSlash)) {
						typeInfo = parameters.get(i).a;
						break;
					}
				}

				JVMInstructions.getStatic(codeContent, constantIndices, MuseUtils.fieldName(sharedData.className, baseClass));
				ConstantPoolManager.createFieldReferenceNonLocal(sharedData, sharedData.objectTypes.get(baseClass), afterSlash, ContainerVariableInfo.typeToStringRepresentation(typeInfo, ""));
				JVMInstructions.getField(codeContent, sharedData.constantIndices.get(ConstantType.FieldReference).get(MuseUtils.fieldName(sharedData.objectTypes.get(baseClass), afterSlash)));
			}
			return true;
		} else {
			return false;
		}
	}

	public static void loadValue(ClassFileSharedData sharedData, int lineNum, String variableName, MyLinkedList<Byte> codeContent,
								 Map<String, Pair<Integer, VariableType>> variablesInUse, ContainerBlock currentContainer) {
		loadValue(sharedData, lineNum, variableName, codeContent, variablesInUse, sharedData.globalVariables, sharedData.constantIndices, currentContainer);
	}
	public static void loadValue(ClassFileSharedData sharedData, int lineNum, String variableName, MyLinkedList<Byte> codeContent,
								 Map<String, Pair<Integer, VariableType>> variablesInUse,
								 Map<String, Triple<Integer, VariableType, Boolean>> staticVariablesInUse,
								 Map<ConstantType, Map<String, Integer>> constantIndices,
								 ContainerBlock currentContainer) {

		if (!tryLoadValue(sharedData, variableName, codeContent, variablesInUse, staticVariablesInUse, constantIndices, currentContainer)) {
			throw new RuntimeException("Line " + lineNum + ": Unable to find variable '" + variableName + "'.");
		}
	}

	// Creates code to process the arithmetic expression and dump it onto the stack
	// Returns the stack space required to do so
	public static int loadArithmeticExpression(ClassFileSharedData sharedData, MyLinkedList<Byte> codeContent,
											   Map<String, Pair<Integer, VariableType>> variablesInUse,
											   String arithmeticExpression,
											   Map<String, Triple<Integer, VariableType, Boolean>> staticVariablesInUse,
											   Map<ConstantType, Map<String, Integer>> constantIndices,
											   ContainerBlock currentContainer) {

		arithmeticExpression = MuseUtils.trim(arithmeticExpression);

		// Existing variable
		if (tryLoadValue(sharedData, arithmeticExpression, codeContent, variablesInUse, staticVariablesInUse, constantIndices, currentContainer)) {
			return 1;
		}

		// Arithmetic expression
		if (arithmeticExpression.contains("+")) {
			loadArithmeticExpression(sharedData, codeContent, variablesInUse, arithmeticExpression.substring(0, arithmeticExpression.indexOf("+")), staticVariablesInUse, constantIndices, currentContainer);
			loadArithmeticExpression(sharedData, codeContent, variablesInUse, arithmeticExpression.substring(arithmeticExpression.indexOf("+") + 1), staticVariablesInUse, constantIndices, currentContainer);
			codeContent.add((byte)96); // iadd
			return 2;

		// Negative values can be misinterpreted as equations so check for that
		} else if (!MuseUtils.isInt(arithmeticExpression) && arithmeticExpression.contains("-")) {
			loadArithmeticExpression(sharedData, codeContent, variablesInUse, arithmeticExpression.substring(0, arithmeticExpression.indexOf("-")), staticVariablesInUse, constantIndices, currentContainer);
			loadArithmeticExpression(sharedData, codeContent, variablesInUse, arithmeticExpression.substring(arithmeticExpression.indexOf("-") + 1), staticVariablesInUse, constantIndices, currentContainer);
			codeContent.add((byte)100); // isub
			return 2;

		// Negative values can be misinterpreted as equations so check for that
		} else if (arithmeticExpression.contains("*")) {
			loadArithmeticExpression(sharedData, codeContent, variablesInUse, arithmeticExpression.substring(0, arithmeticExpression.indexOf("*")), staticVariablesInUse, constantIndices, currentContainer);
			loadArithmeticExpression(sharedData, codeContent, variablesInUse, arithmeticExpression.substring(arithmeticExpression.indexOf("*") + 1), staticVariablesInUse, constantIndices, currentContainer);
			codeContent.add((byte)104); // imul
			return 2;

		// Negative values can be misinterpreted as equations so check for that
		} else if (arithmeticExpression.contains("/")) {
			loadArithmeticExpression(sharedData, codeContent, variablesInUse, arithmeticExpression.substring(0, arithmeticExpression.indexOf("/")), staticVariablesInUse, constantIndices, currentContainer);
			loadArithmeticExpression(sharedData, codeContent, variablesInUse, arithmeticExpression.substring(arithmeticExpression.indexOf("/") + 1), staticVariablesInUse, constantIndices, currentContainer);
			codeContent.add((byte)108); // idiv
			return 2;
		}

		// Integer literal
		if (!MuseUtils.isInt(arithmeticExpression)) {
			throw new RuntimeException(arithmeticExpression + " is not an integer and not defined!");
		}
		JVMInstructions.pushInt(codeContent, Integer.parseInt(arithmeticExpression));
		return 1;
	}


	public static int loadExpressionAndWriteImmediately(
			ClassFileSharedData sharedData,
			MyLinkedList<Byte> codeContent,
			Map<String, Pair<Integer, VariableType>> variablesInUse,
			String expression,
			int lineNumber,
			ContainerBlock currentContainer,
			Map<String, String> variableTypeNames) {

		return loadExpressionAndWriteImmediately(sharedData, sharedData.constantPool, sharedData.constantIndices, codeContent,
				variablesInUse, sharedData.globalVariables, expression, sharedData.containerSignatures,
				lineNumber, sharedData.className, currentContainer, variableTypeNames);
	}
	public static int loadExpressionAndWriteImmediately(
			ClassFileSharedData sharedData,
			List<byte[]> constantPool,
			Map<ConstantType, Map<String, Integer>> constantIndices,
			MyLinkedList<Byte> codeContent,
			Map<String, Pair<Integer, VariableType>> variablesInUse,
			Map<String, Triple<Integer, VariableType, Boolean>> staticVariablesInUse,
			String expression,
			Map<String, ContainerVariableInfo> containerInfo,
			int lineNumber,
			String className,
			ContainerBlock currentContainer,
			Map<String, String> variableTypeNames) {

		Triple<VariableType, MyLinkedList<Byte>, Integer> result = loadExpression(sharedData, constantPool, constantIndices,
				variablesInUse, staticVariablesInUse, expression, containerInfo, lineNumber, className, currentContainer, variableTypeNames);
		codeContent.append(result.b);
		return result.c;
	}

	// Returns bytes to evaluate the given expression/push to result to the stack, the type of the expression, and the
	// required stack size
	public static Triple<VariableType, MyLinkedList<Byte>, Integer> loadExpression(
			ClassFileSharedData sharedData,
			Map<String, Pair<Integer, VariableType>> variablesInUse,
			String expression,
			int lineNumber, ContainerBlock currentContainer, Map<String, String> variableTypeNames) {
		return loadExpression(sharedData, sharedData.constantPool, sharedData.constantIndices, variablesInUse, sharedData.globalVariables, expression, sharedData.containerSignatures, lineNumber, sharedData.className, currentContainer, variableTypeNames);
	}
	public static Triple<VariableType, MyLinkedList<Byte>, Integer> loadExpression(
			ClassFileSharedData sharedData,
			List<byte[]> constantPool,
			Map<ConstantType, Map<String, Integer>> constantIndices,
			Map<String, Pair<Integer, VariableType>> variablesInUse,
			Map<String, Triple<Integer, VariableType, Boolean>> staticVariablesInUse,
			String expression,
			Map<String, ContainerVariableInfo> containerInfo,
			int lineNumber,
			String className, ContainerBlock currentContainer, Map<String, String> variableTypeNames) {

		expression = MuseUtils.trim(expression);

		if (expression.startsWith("(") && expression.endsWith(")")) {
			expression = expression.substring(1, expression.length() - 1);
			expression = MuseUtils.trim(expression);
		}

		int requiredStackSize = 0;
		MyLinkedList<Byte> newCommands = new MyLinkedList<Byte>();
		VariableType type = null;

		// Existing variable
		Pair<Integer, VariableType> pair = variablesInUse.get(expression);
		if (pair != null) {
			JVMInstructions.iLoad(newCommands, pair.a);
			return new Triple<VariableType, MyLinkedList<Byte>, Integer>(pair.b, newCommands, 1);
		} else {
			Triple<Integer, VariableType, Boolean> staticInfo = staticVariablesInUse.get(expression);
			if (staticInfo != null) {
				pair = new Pair<Integer, VariableType>(staticInfo.a, staticInfo.b);
			}

			if (staticInfo != null && currentContainer.ourStaticVariables.contains(expression)) {
				JVMInstructions.getStatic(newCommands, constantIndices, MuseUtils.fieldName(sharedData.className, expression));
				return new Triple<VariableType, MyLinkedList<Byte>, Integer>(pair.b, newCommands, 1);
			}
		}

		if (expression.contains("<=")) {
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple1 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(0, expression.indexOf("<=")), containerInfo, lineNumber, className, currentContainer, variableTypeNames);
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple2 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(expression.indexOf("<=") + 2), containerInfo, lineNumber, className, currentContainer, variableTypeNames);

			if (triple1.a != triple2.a) {
				throw new RuntimeException("Mismatched types for expression " + expression + " on line " + lineNumber + ": " + triple1.a + " and " + triple2.a);
			}

			if (triple1.a == VariableType.Int) {
				type = triple1.a;

				newCommands.append(triple1.b);
				newCommands.append(triple2.b);
				newCommands.add((byte) 163);

				requiredStackSize += triple1.c + triple2.c + 1;
				return new Triple<VariableType, MyLinkedList<Byte>, Integer>(type, newCommands, requiredStackSize);
			}
		// String
		} else if (expression.startsWith("'") || expression.startsWith("\"")) {

			// Save constant string
			int index = ConstantPoolManager.createStringConstant(constantPool, expression.substring(1, expression.length() - 1), constantIndices);

			type = VariableType.String;
			requiredStackSize = 1;
			JVMInstructions.loadConstant(newCommands, index);
			return new Triple<VariableType, MyLinkedList<Byte>, Integer>(type, newCommands, requiredStackSize);

		// Int
		} else if (MuseUtils.isInt(expression)) {

			type = VariableType.Int;
			requiredStackSize = 1;
			JVMInstructions.pushInt(newCommands, Integer.parseInt(expression));

		} else if (expression.contains("+")) {
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple1 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(0, expression.indexOf("+")), containerInfo, lineNumber, className, currentContainer, variableTypeNames);
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple2 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(expression.indexOf("+") + 1), containerInfo, lineNumber, className, currentContainer, variableTypeNames);

			if (triple1.a != triple2.a) {
				throw new RuntimeException("Mismatched types for expression " + expression + " on line " + lineNumber + ": " + triple1.a + " and " + triple2.a);
			}
			type = triple1.a;

			newCommands.append(triple1.b);
			newCommands.append(triple2.b);
			newCommands.add((byte)96);

			requiredStackSize += triple1.c + triple2.c + 1;
			return new Triple<VariableType, MyLinkedList<Byte>, Integer>(type, newCommands, requiredStackSize);

		} else if (expression.contains("*")) {
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple1 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(0, expression.indexOf("*")), containerInfo, lineNumber, className, currentContainer, variableTypeNames);
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple2 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(expression.indexOf("*") + 1), containerInfo, lineNumber, className, currentContainer, variableTypeNames);

			if (triple1.a != triple2.a) {
				throw new RuntimeException("Mismatched types for expression " + expression + " on line " + lineNumber + ": " + triple1.a + " and " + triple2.a);
			}
			type = triple1.a;

			newCommands.append(triple1.b);
			newCommands.append(triple2.b);
			newCommands.add((byte)104);

			requiredStackSize += triple1.c + triple2.c + 1;
			return new Triple<VariableType, MyLinkedList<Byte>, Integer>(type, newCommands, requiredStackSize);

		} else if (expression.contains("%")) {
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple1 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(0, expression.indexOf("%")), containerInfo, lineNumber, className, currentContainer, variableTypeNames);
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple2 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(expression.indexOf("%") + 1), containerInfo, lineNumber, className, currentContainer, variableTypeNames);

			if (triple1.a != triple2.a) {
				throw new RuntimeException("Mismatched types for expression " + expression + " on line " + lineNumber + ": " + triple1.a + " and " + triple2.a);
			}
			type = triple1.a;

			newCommands.append(triple1.b);
			newCommands.append(triple2.b);
			newCommands.add((byte)112);

			requiredStackSize += triple1.c + triple2.c + 1;
			return new Triple<VariableType, MyLinkedList<Byte>, Integer>(type, newCommands, requiredStackSize);
		}

		if (expression.contains("-")) {
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple1 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(0, expression.indexOf("-")), containerInfo, lineNumber, className, currentContainer, variableTypeNames);
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple2 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(expression.indexOf("-") + 1), containerInfo, lineNumber, className, currentContainer, variableTypeNames);

			if (triple1.a == triple2.a) {
				type = triple1.a;

				newCommands.append(triple1.b);
				newCommands.append(triple2.b);
				newCommands.add((byte)100);

				requiredStackSize += triple1.c + triple2.c + 1;
				return new Triple<VariableType, MyLinkedList<Byte>, Integer>(type, newCommands, requiredStackSize);
			}
		}

		if (expression.contains("/")) {
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple1 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(0, expression.indexOf("/")), containerInfo, lineNumber, className, currentContainer, variableTypeNames);
			Triple<VariableType, MyLinkedList<Byte>, Integer> triple2 =
					loadExpression(sharedData, constantPool, constantIndices, variablesInUse, staticVariablesInUse,
							expression.substring(expression.indexOf("/") + 1), containerInfo, lineNumber, className, currentContainer, variableTypeNames);

			if (triple1.a != null && triple2.a != null) {
				if (triple1.a != triple2.a) {
					throw new RuntimeException("Mismatched types for expression " + expression + " on line " + lineNumber + ": " + triple1.a + " and " + triple2.a);
				}

				if (triple1.a == VariableType.Int) {
					type = triple1.a;

					newCommands.append(triple1.b);
					newCommands.append(triple2.b);
					newCommands.add((byte) 108);

					requiredStackSize += triple1.c + triple2.c + 1;
					return new Triple<VariableType, MyLinkedList<Byte>, Integer>(type, newCommands, requiredStackSize);
				}
			}
		}

		// Array index
		if (expression.endsWith("]") && expression.contains("[")) {
			String baseVariable = expression.substring(0, expression.indexOf("["));
			String index = expression.substring(expression.indexOf("[") + 1, expression.length() - 1);

			Pair<Integer, VariableType> typeInfo = variablesInUse.get(baseVariable);

			// Load reference
			newCommands.add((byte)42); // aload_0
			//codeContent.add(typeInfo.a.byteValue());

			// Load offset
			int stackSpaceRequired = loadArithmeticExpression(sharedData, newCommands, variablesInUse, index + "-1", staticVariablesInUse, constantIndices, currentContainer);
			requiredStackSize = Math.max(requiredStackSize, 1 + stackSpaceRequired);

			// Go to that position in the array
			newCommands.add((byte)50);

			type = VariableType.String;

		// Count
		} else if (expression.endsWith("/count")) {
			String baseVariable = expression.substring(0, expression.indexOf("/count"));

			// Load reference
			newCommands.add((byte)42); // aload_0

			// Get array length (yes, there is a dedicated command for that)
			newCommands.add((byte)190); // arraylength

			type = VariableType.Int;
			requiredStackSize = 1;
		}

		return new Triple<VariableType, MyLinkedList<Byte>, Integer>(type, newCommands, requiredStackSize);
	}

}
