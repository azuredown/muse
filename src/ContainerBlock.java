import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.misc.Triple;

import java.util.*;

// A block of code that can be a class, function, or method.
public class ContainerBlock {

	public int startLine, endLine;

	public ContainerBlock parentContainer;

	private int expectedNumIndents = 0;

	public Set<String> ourStaticVariables = new HashSet<String>();

	// The top value on the stack is the location of the last symbol causing an integer
	// Contains address of start, indent reason, and a (possibly null) condition to check before jumping
	private Stack<IndentInformation> reasonsForIndents = new Stack<IndentInformation>();

	// Whenever we do something that requires stack space make sure this value is updated properly
	public int requiredStackSize = 0;

	public MyLinkedList<Byte> codeContent = new MyLinkedList<Byte>();
	//public List<Integer> stackMapValues = new ArrayList<Integer>();

	// String -> Index in array, Type
	private Map<String, Pair<Integer, VariableType>> variablesInUse = new HashMap<String, Pair<Integer, VariableType>>();

	public int numVariablesInUse() { return variablesInUse.size(); }

	// Label -> Instruction index
	public Map<String, LinkedListNode<Byte>> gotos = new HashMap<String, LinkedListNode<Byte>>();
	public Map<String, LinkedListNode<Byte>> labels = new HashMap<String, LinkedListNode<Byte>>();

	// For generating the stackmap
	public List<byte[]> stackData = new ArrayList<byte[]>();
	// The address stored in the stackmap is the current address - the last address - 1 for all but the first address
	// which is just the offset.
	private int lastStackAddress = -1;
	private Map<VariableType, Integer> typesUsedAtLastStackAddress;

	public List<ContainerBlock> childContainers = new ArrayList<ContainerBlock>();

	public String name;

	private ClassFileSharedData sharedData;

	public ContainerVariableInfo variableInfo;

	private boolean isObject;


	public ContainerBlock(String name, ClassFileSharedData sharedData, ContainerVariableInfo variableInfo,
						  int startLine, boolean isObject) {

		ConstantPoolManager.writeConstantValue(sharedData, name);
		this.sharedData = sharedData;
		this.startLine = startLine;
		this.name = name;
		this.isObject = isObject;

		if (isObject) {
			// Apparently we need to store a reference to ourself for some reason.
			// Will probably have to find a better solution to do this later.
			variablesInUse.put("..", new Pair<Integer, VariableType>(variablesInUse.size(), VariableType.Object));
		}

		for (int i = 0; i < variableInfo.parameters.size(); i++) {
			variablesInUse.put(variableInfo.parameters.get(i).b, new Pair<Integer, VariableType>(variablesInUse.size(), variableInfo.parameters.get(i).a ));
		}

		typesUsedAtLastStackAddress = MuseUtils.countVariables(variablesInUse);
		this.variableInfo = variableInfo;
		ConstantPoolManager.writeConstantValue(sharedData, variableInfo.makeTypeSignature());
	}


	public void notifyDone(int lineNumber) {

		this.endLine = lineNumber - 1;

		for (String label : gotos.keySet()) {
			JVMInstructions.gotoPermanent(codeContent, gotos.get(label), codeContent.getIndex(labels.get(label)) - codeContent.getIndex(gotos.get(label)));
		}

		// End all if conditions if they haven't already ended
		checkEndedIntents(0, lineNumber, false);

		// Return.
		if (variableInfo.returnType == null) {
			if (codeContent.last == null || codeContent.last.item != (byte)-79) {
				codeContent.add((byte) -79);
			} else {
				// If we already added a return it means the accompanying stack data for that return is invalid so
				// remove it
				stackData.remove(stackData.size() - 1);
			}
		} else {
			// If we already added a return it means the accompanying stack data for that return is invalid so
			// remove it
			stackData.remove(stackData.size() - 1);
		}

		// TODO: Assert that we have a return statement at the end of every function. JVM will check this automatically but better safe than sorry.
	}

	public void notifyBreak(String returnString, int lineNum) {

		checkIndents(returnString, lineNum);

		if (variableInfo.returnType == null) {
			codeContent.add((byte) -79);

			createStackEntry(codeContent.size, lineNum);
		}

		// For some reason if we have unreachable code we need to call super again. Don't know why.
		if (isObject) {
			callSuper("java/lang/Object");
		}
	}

	public void notifyPrint(MuseParser.Print_opContext ctx, List<String> tokens) {

		int indents = checkIndents(tokens.get(0), ctx.start.getLine());

		String toPrint = MuseUtils.trim(tokens.get(0).substring(indents + 5));

		//for (int i = 1; i < tokens.size(); i++) {

		Triple<VariableType, MyLinkedList<Byte>, Integer> typeData =
				LoadManager.loadExpression(sharedData, variablesInUse, toPrint, ctx.start.getLine(), this, sharedData.objectTypes);

		// String constant
		if (typeData.a == VariableType.String) {
			ConstantPoolManager.importPrintlnString(sharedData);

			// Print constant string
			JVMInstructions.getStatic(codeContent, sharedData.constantIndices, MuseUtils.fieldName("java/lang/System", "out"));
			// Plus 1 because get static pushes a command on the stack
			requiredStackSize = Math.max(typeData.c + 1, requiredStackSize);
			codeContent.append(typeData.b);
			JVMInstructions.invokeVirtual(codeContent, sharedData.constantIndices, "java/io/PrintStream/println:(Ljava/lang/String;)V");

		// Int constant
		} else if (typeData.a == VariableType.Int) {
			ConstantPoolManager.importPrintlnInt(sharedData);

			// This is the new way to print a number.
			JVMInstructions.getStatic(codeContent, sharedData.constantIndices, MuseUtils.fieldName("java/lang/System", "out"));
			// Plus 1 because get static pushes a command on the stack
			requiredStackSize = Math.max(typeData.c + 1, requiredStackSize);
			codeContent.append(typeData.b);
			JVMInstructions.invokeVirtual(codeContent, sharedData.constantIndices, "java/io/PrintStream/println:(I)V");

		// Boolean value
		} else if (toPrint.equals("true") || toPrint.equals("false")) {
			ConstantPoolManager.importPrintlnBool(sharedData);

			// This is the new way to print a number.
			JVMInstructions.getStatic(codeContent, sharedData.constantIndices, MuseUtils.fieldName("java/lang/System", "out"));
			// Plus 1 because get static pushes a command on the stack
			requiredStackSize = Math.max(typeData.c + 1, requiredStackSize);
			JVMInstructions.pushInt(codeContent, toPrint.equals("true") ? 1 : 0);
			JVMInstructions.invokeVirtual(codeContent, sharedData.constantIndices, "java/io/PrintStream/println:(Z)V");

		// Function
		} else if (sharedData.functionDefined(toPrint)) {

			if (!toPrint.contains("/")) {
				throw new RuntimeException("Line: " + ctx.start.getLine() + " cannot print class yet.");
			}

			String variableToGet = MuseUtils.trim(toPrint.substring(toPrint.indexOf("/") + 1));
			toPrint = MuseUtils.trim(toPrint.substring(0, toPrint.indexOf("/")));

			String functionName = toPrint.contains(" ") ? toPrint.substring(0, toPrint.indexOf(" ")) : toPrint;
			String arguments = toPrint.contains(" ") ? toPrint.substring(toPrint.indexOf(" ") + 1) : "";
			arguments = MuseUtils.trim(arguments);

			VariableType type = null;
			List<Pair<VariableType, String>> parameters = sharedData.containerSignatures.get(functionName).parameters;
			for (int i = 0; i < parameters.size(); i++) {
				if (parameters.get(i).b.equals(variableToGet)) {
					type = parameters.get(i).a;
					break;
				}
			}

			if (type == null) {
				throw new RuntimeException("Line " + ctx.start.getLine() + ": cannot print null.");
			} else {
				// Call the function and then print the result
				switch (type) {
					case Int:
						ConstantPoolManager.importPrintlnInt(sharedData);
						JVMInstructions.getStatic(codeContent, sharedData.constantIndices, MuseUtils.fieldName("java/lang/System", "out"));
						// 2 because get static pushes a command on the stack
						requiredStackSize = Math.max(2, requiredStackSize);

						List<String> argumentList = new ArrayList<String>();
						argumentList.add(arguments);
						notifyFunctionCall(ctx.start.getLine(), functionName, argumentList, 1, true);

						ConstantPoolManager.createFieldReferenceNonLocal(sharedData, functionName,
								variableToGet, "I");
						JVMInstructions.getField(codeContent, sharedData.constantIndices.get(ConstantType.FieldReference).
								get(MuseUtils.fieldName(functionName, variableToGet)));

						JVMInstructions.invokeVirtual(codeContent, sharedData.constantIndices, "java/io/PrintStream/println:(I)V");
						break;
					default:
						throw new RuntimeException("Unknown type: " + type);
				}
			}

		// Variable
		} else {

			// Get information for the type
			VariableType typeInfo = null;

			String index = null;
			if (toPrint.endsWith("]") && toPrint.contains("[")) {
				String baseVariable = toPrint.substring(0, toPrint.indexOf("["));
				index = toPrint.substring(toPrint.indexOf("[") + 1, toPrint.length() - 1);

				if (MuseUtils.isInt(index)) {
					typeInfo = variablesInUse.get(baseVariable).b;
				}
			}
			ContainerBlock blockForStaticVariable = this;
			if (typeInfo == null) {

				while (toPrint.startsWith("../")) {
					toPrint = toPrint.substring(3);
					blockForStaticVariable = blockForStaticVariable.parentContainer;
				}

				String afterSlash = "";
				String baseClass = "";
				if (toPrint.contains("/")) {
					afterSlash = toPrint.substring(toPrint.indexOf("/") + 1);
					baseClass = toPrint.substring(0, toPrint.indexOf("/"));
				} else {
					baseClass = toPrint;
				}

				if (variablesInUse.containsKey(baseClass)) {
					typeInfo = variablesInUse.get(baseClass).b;
				} else if (sharedData.hasStaticVariable(baseClass)) {

					if (!blockForStaticVariable.ourStaticVariables.contains(baseClass)) {
						throw new RuntimeException("Line " + ctx.start.getLine() + ": Trying to access variable '" + baseClass + "' using the incorrect container '" + blockForStaticVariable.name + "'.");
					}

					if (afterSlash.equals("")) {
						Triple<Integer, VariableType, Boolean> staticInfo = sharedData.staticVarType(baseClass);
						typeInfo = new Pair<Integer, VariableType>(staticInfo.a, staticInfo.b).b;
					} else {
						List<Pair<VariableType, String>> parameters = sharedData.containerSignatures.get(sharedData.objectTypes.get(baseClass)).parameters;
						for (int i = 0; i < parameters.size(); i++) {
							if (parameters.get(i).b.equals(afterSlash)) {
								typeInfo = parameters.get(i).a;
								break;
							}
						}
					}
				} else {
					throw new RuntimeException(sharedData.className + " line " + ctx.start.getLine() + ": Unable to find variable or function '" + baseClass + "'.");
				}
			}

			switch (typeInfo) {
				case Int:
					ConstantPoolManager.importPrintlnInt(sharedData);
					JVMInstructions.getStatic(codeContent, sharedData.constantIndices, MuseUtils.fieldName("java/lang/System", "out"));
					// 2 because get static pushes a command on the stack
					requiredStackSize = Math.max(2, requiredStackSize);
					LoadManager.loadValue(sharedData, ctx.start.getLine(), toPrint, codeContent, variablesInUse, blockForStaticVariable);
					JVMInstructions.invokeVirtual(codeContent, sharedData.constantIndices, "java/io/PrintStream/println:(I)V");
					break;
				case Bool:
					ConstantPoolManager.importPrintlnBool(sharedData);
					JVMInstructions.getStatic(codeContent, sharedData.constantIndices, MuseUtils.fieldName("java/lang/System", "out"));
					// 2 because get static pushes a command on the stack
					requiredStackSize = Math.max(2, requiredStackSize);
					LoadManager.loadValue(sharedData, ctx.start.getLine(), toPrint, codeContent, variablesInUse, blockForStaticVariable);
					JVMInstructions.invokeVirtual(codeContent, sharedData.constantIndices, "java/io/PrintStream/println:(Z)V");
					break;
				default:
					throw new RuntimeException("Unknown type: " + typeInfo);
			}
		}
	}

	public void callSuper(String parentClass) {

		ConstantPoolManager.createMethodReferenceConstant(sharedData, parentClass, "<init>", "()V", false);

		// I think the aload is loading a reference to ourself, but I'm not 100% sure.
		JVMInstructions.aload(codeContent, 0);
		JVMInstructions.invokeSpecial(codeContent, sharedData.constantIndices, parentClass + ".<init>:()V");

		requiredStackSize = Math.max(requiredStackSize, 1);
	}

	public void notifyFunctionCall(int lineNumber, String functionName, List<String> arguments, int existingStackSize, boolean dup) {
		instantiate(lineNumber, functionName, arguments, dup, existingStackSize);
	}

	public void notifyLabel(MuseParser.Label_opContext ctx, List<String> tokens) {
		codeContent.addEmptyNode();
		labels.put(MuseUtils.trim(tokens.get(0).substring(5)), codeContent.last);
		createStackEntry(codeContent.size, ctx.start.getLine());
	}

	public void notifyGoto(MuseParser.Goto_opContext ctx, List<String> tokens) {

		codeContent.addEmptyNode();
		gotos.put(MuseUtils.trim(tokens.get(0).substring(4)), codeContent.last);
		JVMInstructions.gotoTemporary(codeContent);

		// For some reason we need to create a stack entry for the command immediately after the goto. No idea why.
		createStackEntry(codeContent.size, ctx.start.getLine());
	}

	public void notifyIncrementDecrement(MuseParser.IncrementDecrement_opContext ctx, List<String> tokens) {

		int indents = checkIndents(tokens.get(0), ctx.start.getLine());
		String string = indents == 0 ? tokens.get(0) : tokens.get(0).substring(indents);

		if (tokens.get(0).contains("--")) {
			increment( ctx.start.getLine(), -1, string.substring(0, string.indexOf("--")));
		} else {
			increment( ctx.start.getLine(), 1, string.substring(0, string.indexOf("++")));
		}

	}

	public void notifyOperationEquals(int lineNum, List<String> tokens) {

		String string = tokens.get(0);

		if (string.contains("+=")) {
			String variable = MuseUtils.trim(string.substring(0, string.indexOf("+=")));
			String number = MuseUtils.trim(string.substring(string.indexOf("+=") + 2));

			increment(lineNum, Integer.parseInt(number), variable);
			return;
		} else if (string.contains("-=")) {
			String variable = MuseUtils.trim(string.substring(0, string.indexOf("-=")));
			String number = MuseUtils.trim(string.substring(string.indexOf("-=") + 2));

			increment(lineNum, -Integer.parseInt(number), variable);
			return;
		} else if (string.contains("*=")) {
			String variable = MuseUtils.trim(string.substring(0, string.indexOf("*=")));
			String number = MuseUtils.trim(string.substring(string.indexOf("*=") + 2));

			// Load variable, multiply, then save
			LoadManager.loadValue(sharedData, lineNum, variable, codeContent, variablesInUse, this);
			JVMInstructions.pushInt(codeContent, Integer.parseInt(number));
			requiredStackSize = Math.max(2, requiredStackSize);
			JVMInstructions.multiplyInt(codeContent);
			assignValueToVariable(lineNum, variable, VariableType.Int);
			return;
		} else if (string.contains("/=")) {
			String variable = MuseUtils.trim(string.substring(0, string.indexOf("/=")));
			String number = MuseUtils.trim(string.substring(string.indexOf("/=") + 2));

			// Load variable, divide, then save
			LoadManager.loadValue(sharedData, lineNum, variable, codeContent, variablesInUse, this);
			JVMInstructions.pushInt(codeContent, Integer.parseInt(number));
			requiredStackSize = Math.max(2, requiredStackSize);
			JVMInstructions.divideInt(codeContent);
			assignValueToVariable(lineNum, variable, VariableType.Int);
			return;
		}
		throw new RuntimeException("Unknown operator: " + string);
	}

	public void notifyLoop(MuseParser.Loop_opContext ctx, List<String> tokens) {

		int indents = checkIndents(tokens.get(0), ctx.start.getLine());
		expectedNumIndents++;

		String text = indents == 0 ? tokens.get(0) : tokens.get(0).substring(indents);

		// Get the contents between if and (optional) :
		String condition = text.substring(text.indexOf("while") + 5);
		condition = MuseUtils.trim(condition);
		if (condition.endsWith(":")) {
			condition = condition.substring(0, condition.length() - 1);
		}

		if (text.startsWith("do")) {

			List<LinkedListNode<Byte>> addresses = new ArrayList<LinkedListNode<Byte>>();
			codeContent.addEmptyNode();
			addresses.add(codeContent.last);

			reasonsForIndents.push(new IndentInformation(addresses.get(0), addresses, IndentReason.DoWhile, condition, null));
			createStackEntry(codeContent.size, ctx.start.getLine());

		} else {

			createStackEntry(codeContent.size, ctx.start.getLine());

			codeContent.addEmptyNode();
			LinkedListNode<Byte> firstAddress = codeContent.last;
			List<LinkedListNode<Byte>> addresses = processCondition(condition, ctx.start.getLine(), (byte)153);

			reasonsForIndents.push(new IndentInformation(firstAddress, addresses, IndentReason.While, null, null));
		}
	}

	public void assignValueToGlobalVariable(String variable) {
		ConstantPoolManager.createGlobalVariable(sharedData, variable, ContainerVariableInfo.typeToStringRepresentation(variablesInUse.get(variable).b, ""));
		JVMInstructions.aload(codeContent, variablesInUse.get("..").a);
		LoadManager.loadValue(sharedData, 0, variable, codeContent, variablesInUse, this);
		sharedData.addStaticVar(sharedData.className, variable, VariableType.Int, false);
		ourStaticVariables.add(variable);
		requiredStackSize = Math.max(2, requiredStackSize);
		JVMInstructions.putField(codeContent, sharedData.constantIndices.get(ConstantType.FieldReference).get(MuseUtils.fieldName(sharedData.className, variable)));
	}

	// Assigns the variable the value that is currently the top thing in the stack
	private void assignValueToVariable(int lineNum, String variableName, VariableType type, String typeNameIfObject) {

		if (gotos.containsKey(variableName) || labels.containsKey(variableName)) {
			throw new RuntimeException("Variable " + variableName + " is already used for a label.");
		}

		// If not root save to local variable
		if (parentContainer != null) {
			if (!variablesInUse.containsKey(variableName)) {
				variablesInUse.put(variableName, new Pair<Integer, VariableType>(variablesInUse.size(), type));
			} else {
				if (variablesInUse.get(variableName).b != type) {
					throw new RuntimeException("Line " + lineNum + ": Type conflict for variable " + variableName + ". Previously set to " + variableName + " but now being set to " + type + ".");
				}
			}

			JVMInstructions.iStore(codeContent, variablesInUse.get(variableName).a);

		// Else save to static reference
		} else {
			ConstantPoolManager.createGlobalVariable(sharedData, variableName, ContainerVariableInfo.typeToStringRepresentation(type, typeNameIfObject));

			if (!sharedData.hasStaticVariable(variableName)) {
				ourStaticVariables.add(variableName);
				sharedData.addStaticVar(sharedData.className, variableName, type, true);
			} else {
				if (sharedData.staticVarType(variableName).b != type) {
					throw new RuntimeException("Line " + lineNum + ": Type conflict for variable " + variableName + ". Previously set to " + variableName + " but now being set to " + type + ".");
				} else if (!ourStaticVariables.contains(variableName)) {
					throw new RuntimeException("Line " + lineNum + ": Trying to override the static variable '" + variableName + "'.");
				}
			}

			JVMInstructions.putStatic(codeContent, sharedData.staticVarType(variableName).a);
		}
	}
	private void assignValueToVariable(int lineNum, String variableName, VariableType type) {
		assignValueToVariable(lineNum, variableName, type, "");
	}

	public void notifyAssignment(MuseParser.Assignment_opContext ctx, List<String> tokens) {

		int indents = checkIndents(tokens.get(0), ctx.start.getLine());
		if (indents != 0) {
			tokens.set(0, tokens.get(0).substring(indents));
		}

		if (tokens.size() != 1) {
			throw new RuntimeException("Incorrect number of tokens in assignment statement! Expected 1 but got " + tokens.size() + ".");
		}
		if (!tokens.get(0).contains("<-")) {
			throw new RuntimeException("Unable to find <- in assignment: " + tokens.get(0));
		}
		String[] subTokens = {
				MuseUtils.trim(tokens.get(0).substring(0, tokens.get(0).indexOf("<-"))),
				MuseUtils.trim(tokens.get(0).substring(tokens.get(0).indexOf("<-") + 2)) };

		if (subTokens[1].equals("true")) {
			JVMInstructions.pushInt(codeContent, 1);
			assignValueToVariable(ctx.start.getLine(), subTokens[0], VariableType.Bool);
			return;

		} else if (subTokens[1].equals("false")) {
			JVMInstructions.pushInt(codeContent, 0);
			assignValueToVariable(ctx.start.getLine(), subTokens[0], VariableType.Bool);
			return;

		} else if ((!subTokens[1].contains(" ") && sharedData.containerSignatures.containsKey(subTokens[1])) ||
					(subTokens[1].contains(" ") && sharedData.containerSignatures.containsKey(subTokens[1].substring(0, subTokens[1].indexOf(" "))))) {

			String className = instantiate(ctx.start.getLine(), subTokens[1], true, 0);

			sharedData.objectTypes.put(subTokens[0], className);

			assignValueToVariable(ctx.start.getLine(), subTokens[0], VariableType.Object, className);
			return;
		}

		requiredStackSize = Math.max(LoadManager.loadExpressionAndWriteImmediately(sharedData,
				codeContent, variablesInUse, subTokens[1], ctx.start.getLine(), this, sharedData.objectTypes), requiredStackSize);
		assignValueToVariable(ctx.start.getLine(), subTokens[0], VariableType.Int);
	}

	// Instantiates a new object, initializes it, and leaves a reference to it on the stack (if leaveReferenceOnStack is true).
	// Returns the class name.
	private String instantiate(int lineNum, String functionCallWithArgumentsPreTrimmed, boolean leaveReferenceOnStack, int existingStackSize) {
		List<String> parameters = new ArrayList<String>();
		String className;

		if (functionCallWithArgumentsPreTrimmed.contains(" ")) {
			String parametersAsString = MuseUtils.trim(functionCallWithArgumentsPreTrimmed.substring(functionCallWithArgumentsPreTrimmed.indexOf(" ") + 1));
			className = functionCallWithArgumentsPreTrimmed.substring(0, functionCallWithArgumentsPreTrimmed.indexOf(" "));

			int counter = 0;
			while (true) {
				if (counter > 100) {
					throw new RuntimeException("Line " + lineNum + ": Infinite loop detected");
				}
				counter++;
				if (parametersAsString.contains(" ")) {
					parameters.add(parametersAsString.substring(0, parametersAsString.indexOf(" ")));
					parametersAsString = MuseUtils.trim(parametersAsString.substring(parametersAsString.indexOf(" ")));
				} else {
					parameters.add(parametersAsString);
					break;
				}
			}
		} else {
			className = functionCallWithArgumentsPreTrimmed;
		}

		return instantiate(lineNum, className, parameters, leaveReferenceOnStack, existingStackSize);
	}
	private String instantiate(int lineNum, String className, List<String> parameters, boolean leaveReferenceOnStack, int existingStackSize) {

		ConstantPoolManager.createClassNameConstant(sharedData, className);
		JVMInstructions.newCommand(codeContent, sharedData.constantIndices, className);

		if (leaveReferenceOnStack) {
			// Dup is because calling init removes an item from the stack so we need a second copy to save
			JVMInstructions.dup(codeContent);
		}
		int stackSizeToAdd = 0;

		for (int i = 0; i < parameters.size(); i++) {
			stackSizeToAdd += LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					parameters.get(i), lineNum, this, sharedData.objectTypes);
		}

		// +2 is for the address of the object duplicated
		requiredStackSize = Math.max(requiredStackSize, stackSizeToAdd + (leaveReferenceOnStack ? 2 : 1) + existingStackSize);

		ConstantPoolManager.createMethodReferenceConstant(sharedData, className, "<init>", sharedData.containerSignatures.get(className).makeTypeSignature(), false);
		JVMInstructions.invokeSpecial(codeContent, sharedData.constantIndices, className + ".<init>:" + sharedData.containerSignatures.get(className).makeTypeSignature());

		return className;
	}

	private void increment(int lineNum, int value, String variableName) {
		Pair<Integer, VariableType> variablePair = variablesInUse.get(variableName);
		if (variablePair != null) {
			if (variablePair.b != VariableType.Int) {
				throw new RuntimeException("Type error.");
			}
			JVMInstructions.incrementByConstant(codeContent, variablePair.a, value);
		} else {
			requiredStackSize = Math.max(LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent,
					variablesInUse, variableName + "+" + value, lineNum, this, sharedData.objectTypes), requiredStackSize);
			assignValueToVariable(lineNum, variableName, sharedData.staticVarType(variableName).b);
		}
	}

	// Returns the number of indents
	private int checkIndents(String firstString, int lineNumber) {
		int count = MuseUtils.countIndents(firstString);

		if (count > expectedNumIndents) {
			throw new RuntimeException("Unexpected intent level: " + count);
		}
		checkEndedIntents(count, lineNumber, false);

		return count;
	}
	private IndentInformation checkIndentsAndReturnTopIndentData(String firstString, int lineNumber, boolean encounteredElseOrElif) {
		int count = MuseUtils.countIndents(firstString);

		if (count > expectedNumIndents) {
			throw new RuntimeException("Unexpected intent level: " + count);
		}

		return checkEndedIntents(count, lineNumber, encounteredElseOrElif);
	}

	public void notifyIf(MuseParser.If_opContext ctx, List<String> tokens) {

		int indents = checkIndents(tokens.get(0), ctx.start.getLine());

		expectedNumIndents++;

		// Get the contents between if and (optional) :
		String content = tokens.get(0).substring(indents + 2);
		content = MuseUtils.trim(content);
		if (content.endsWith(":")) {
			content = content.substring(0, content.length() - 1);
		}
		List<LinkedListNode<Byte>> addresses = processCondition(content, ctx.start.getLine(), (byte)153);

		reasonsForIndents.push(new IndentInformation(addresses.get(0), addresses, IndentReason.If, null, null));
	}


	public void notifyElif(int lineNumber, String token) {

		List<LinkedListNode<Byte>> gotoList = new ArrayList<LinkedListNode<Byte>>();

		// Don't make a goto if we already returns
		if (codeContent.last != null && !(codeContent.last.item == (byte)176 || codeContent.last.item == (byte)175 ||
				codeContent.last.item == (byte)174 ||codeContent.last.item == (byte)173 || codeContent.last.item == (byte)172 ||
				codeContent.last.item == (byte)177)) {
			gotoList.add(JVMInstructions.addTemporary(codeContent, (byte) 167));
		}

		// Must be after the goto command inserted earlier
		IndentInformation previousInfo = checkIndentsAndReturnTopIndentData(token, lineNumber, true);
		if (previousInfo.gotos != null) {
			gotoList.addAll(previousInfo.gotos);
		}

		if (previousInfo == null) {
			throw new RuntimeException("Line " + lineNumber + ": previous indent info is null.");
		}
		if (previousInfo.indentReason != IndentReason.Elif && previousInfo.indentReason != IndentReason.If) {
			throw new RuntimeException("Line " + lineNumber + ": unexpected indent reason. Expected if or elif. Got " + previousInfo.indentReason);
		}
		expectedNumIndents++;

		// Get the contents between if and (optional) :
		String content = MuseUtils.trim(token).substring(4);
		content = MuseUtils.trim(content);
		if (content.endsWith(":")) {
			content = MuseUtils.trim(content.substring(0, content.length() - 1));
		}
		List<LinkedListNode<Byte>> addresses = processCondition(content, lineNumber, (byte)153);

		reasonsForIndents.push(new IndentInformation(null, addresses, IndentReason.Elif, null, gotoList));
	}


	public void notifyElse(int lineNumber, String token) {

		List<LinkedListNode<Byte>> gotoList = new ArrayList<LinkedListNode<Byte>>();

		// Don't make a goto if we already returns
		if (codeContent.last != null && !(codeContent.last.item == (byte)176 || codeContent.last.item == (byte)175 ||
				codeContent.last.item == (byte)174 ||codeContent.last.item == (byte)173 || codeContent.last.item == (byte)172 ||
				codeContent.last.item == (byte)177)) {
			gotoList.add(JVMInstructions.addTemporary(codeContent, (byte) 167));
		}

		// Must be after the goto command inserted earlier
		IndentInformation previousInfo = checkIndentsAndReturnTopIndentData(token, lineNumber, true);
		if (previousInfo.gotos != null) {
			gotoList.addAll(previousInfo.gotos);
		}
		expectedNumIndents++;

		reasonsForIndents.push(new IndentInformation(null, null, IndentReason.Else, null, gotoList));
	}


	// Ends all indents that ended and return the last
	private IndentInformation checkEndedIntents(int nestingDepth, int lineNumber, boolean encounteredElseOrElif) {
		IndentInformation indentData = null;

		while (nestingDepth < expectedNumIndents) {
			if (reasonsForIndents.size() == 0) {
				throw new RuntimeException("Line " + lineNumber + ": reason for indents is empty.");
			}
			indentData = reasonsForIndents.pop();
			switch (indentData.indentReason) {
				case If:
					createStackEntry(codeContent.size, lineNumber);
					for (int i = 0; i < indentData.jumpLocations.size(); i++) {
						int offset = codeContent.size - codeContent.getIndex(indentData.jumpLocations.get(i));
						JVMInstructions.ifPermanent(codeContent, indentData.jumpLocations.get(i), offset);
					}
					break;
				case While:
					// Insert a goto command that points to the first if condition
					int offset = codeContent.size - codeContent.getIndex(indentData.startOfIndentBlock);
					JVMInstructions.addGotoPermanent(codeContent, -offset);

					// Link every if condition to the end of the while loop
					for (int i = 0; i < indentData.jumpLocations.size(); i++) {
						offset = codeContent.size - codeContent.getIndex(indentData.jumpLocations.get(i));
						JVMInstructions.ifPermanent(codeContent, indentData.jumpLocations.get(i), offset);
					}

					// Add a stack label right after the goto
					createStackEntry(codeContent.size, lineNumber);
					break;
				case DoWhile:

					List<LinkedListNode<Byte>> gotoConditions = processCondition(indentData.boolCondition, lineNumber, (byte)153);

					// All non-last positions go to the end of the block
					for (int i = 0; i < gotoConditions.size() - 1; i++) {
						addJumpInstruction(codeContent.getIndex(gotoConditions.get(i)), codeContent.size, gotoConditions.get(i));
						createStackEntry(codeContent.size, lineNumber);
					}
					// The last if should go back to the beginning of the block
					// - 3 because we added the goto commands
					byte[] gotoOffset = MuseUtils.intToBytes(codeContent.getIndex(indentData.startOfIndentBlock) - (codeContent.size - 3), 2);

					// This not equals is an optimisation step. If we don't have this we have to use 2 instructions:
					// if and goto. But by inverting the if we can use just one.
					gotoConditions.get(gotoConditions.size() - 1).item = (byte)154;

					gotoConditions.get(gotoConditions.size() - 1).next.item = gotoOffset[0];
					gotoConditions.get(gotoConditions.size() - 1).next.next.item = gotoOffset[1];

					break;
				case Else:

					createStackEntry(codeContent.size, lineNumber);
					for (int i = 0; i < indentData.gotos.size(); i++) {
						gotoOffset = MuseUtils.intToBytes(codeContent.size - codeContent.getIndex(indentData.gotos.get(i)), 2);
						indentData.gotos.get(i).next.item = gotoOffset[0];
						indentData.gotos.get(i).next.next.item = gotoOffset[1];
					}

					break;
				case Elif:

					createStackEntry(codeContent.size, lineNumber);
					for (int i = 0; i < indentData.jumpLocations.size(); i++) {
						offset = codeContent.size - codeContent.getIndex(indentData.jumpLocations.get(i));
						JVMInstructions.ifPermanent(codeContent, indentData.jumpLocations.get(i), offset);
					}
					if (!encounteredElseOrElif) {
						for (int i = 0; i < indentData.gotos.size(); i++) {
							offset = codeContent.size - codeContent.getIndex(indentData.gotos.get(i));
							JVMInstructions.ifPermanent(codeContent, indentData.gotos.get(i), offset);
						}
					}
					break;

				default:
					throw new RuntimeException("Line " + lineNumber + ": Unknown indent reason: " + indentData.indentReason + "." );
			}
			expectedNumIndents--;
		}

		return indentData;
	}


	// Makes code to evaluate the given expression followed by one or more 3 empty byte clusters to populate later with
	// a goto or something.	Returns a list containing the first linked list node of each of the clusters.
	private List<LinkedListNode<Byte>> processCondition(String condition, int lineNumber, byte commandToInsert) {

		List<LinkedListNode<Byte>> returnList = new ArrayList<LinkedListNode<Byte>>();

		condition = MuseUtils.trim(condition);

		if (condition.equals("true")) {
			JVMInstructions.pushInt(codeContent, 1);
			returnList.add(JVMInstructions.addTemporary(codeContent, commandToInsert));

		} else if (condition.equals("false")) {
			JVMInstructions.pushInt(codeContent, 0);
			returnList.add(JVMInstructions.addTemporary(codeContent, commandToInsert));

		} else if (condition.contains(" and ")) {
			returnList.addAll(processCondition(condition.substring(0, condition.indexOf(" and ")), lineNumber, commandToInsert));
			returnList.addAll(processCondition(condition.substring(condition.indexOf(" and ") + 5), lineNumber, commandToInsert));

		} else if (condition.contains(" or ")) {
			List<LinkedListNode<Byte>> previousConditions = processCondition(condition.substring(0, condition.indexOf(" or ")), lineNumber, (byte) 154);
			int positionOfFirstJump = codeContent.size - 3;
			returnList.addAll(processCondition(condition.substring(condition.indexOf(" or ") + 4), lineNumber, commandToInsert));
			createStackEntry(codeContent.size, lineNumber);

			// If first condition passes make it skip to the end of the or block
			addJumpInstruction(positionOfFirstJump, codeContent.size, previousConditions.get(0));

			// Conflicts with = so has to be before that
		} else if (condition.contains(">=")) {
			int newRequiredStackSize = LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(0, condition.indexOf(">=")), lineNumber, this, sharedData.objectTypes);
			newRequiredStackSize += LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(condition.indexOf(">=") + 2), lineNumber, this, sharedData.objectTypes);
			requiredStackSize = Math.max(newRequiredStackSize, requiredStackSize);
			returnList.add(JVMInstructions.addTemporary(codeContent, (byte)161)); // Less than

			// Conflicts with = so has to be before that
		} else if (condition.contains("<=")) {
			int newRequiredStackSize = LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(0, condition.indexOf("<=")), lineNumber, this, sharedData.objectTypes);
			newRequiredStackSize += LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(condition.indexOf("<=") + 2), lineNumber, this, sharedData.objectTypes);
			requiredStackSize = Math.max(newRequiredStackSize, requiredStackSize);
			returnList.add(JVMInstructions.addTemporary(codeContent, (byte)163)); // Greater than

		} else if (condition.contains("=")) {
			int newRequiredStackSize = LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(0, condition.indexOf("=")), lineNumber, this, sharedData.objectTypes);
			newRequiredStackSize += LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(condition.indexOf("=") + 1), lineNumber, this, sharedData.objectTypes);
			requiredStackSize = Math.max(newRequiredStackSize, requiredStackSize);
			returnList.add(JVMInstructions.addTemporary(codeContent, (byte)160));

		} else if (condition.contains("<")) {
			int newRequiredStackSize = LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(0, condition.indexOf("<")), lineNumber, this, sharedData.objectTypes);
			newRequiredStackSize += LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(condition.indexOf("<") + 1), lineNumber, this, sharedData.objectTypes);
			requiredStackSize = Math.max(newRequiredStackSize, requiredStackSize);
			returnList.add(JVMInstructions.addTemporary(codeContent, (byte)162)); // Greater than or equal to

		} else if (condition.contains(">")) {
			int newRequiredStackSize = LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(0, condition.indexOf(">")), lineNumber, this, sharedData.objectTypes);
			newRequiredStackSize += LoadManager.loadExpressionAndWriteImmediately(sharedData, codeContent, variablesInUse,
					condition.substring(condition.indexOf(">") + 1), lineNumber, this, sharedData.objectTypes);
			requiredStackSize = Math.max(newRequiredStackSize, requiredStackSize);
			returnList.add(JVMInstructions.addTemporary(codeContent, (byte)164)); // Less than or equal to

		} else {
			// Variable
			Pair<Integer, VariableType> pair = variablesInUse.get(condition);
			if (pair == null) {
				Triple<Integer, VariableType, Boolean> globalVariable = sharedData.staticVarType(condition);
				if (globalVariable == null) {
					throw new RuntimeException("Line " + lineNumber + ": Unable to find variable " + condition + ".");
				}
				pair = new Pair<Integer, VariableType>(globalVariable.a, globalVariable.b);
			}
			if (pair.b != VariableType.Bool) {
				throw new RuntimeException("Illegal type: " + condition);
			}

			requiredStackSize = Math.max(1, requiredStackSize);
			LoadManager.loadValue(sharedData, lineNumber, condition, codeContent, variablesInUse, this);
			returnList.add(JVMInstructions.addTemporary(codeContent, commandToInsert));
		}

		return returnList;

	}


	private void addJumpInstruction(int comingFrom, int goingTo, LinkedListNode<Byte> nodeOfInstruction) {
		byte[] offset = MuseUtils.intToBytes(goingTo - comingFrom, 2);
		nodeOfInstruction.next.item = offset[0];
		nodeOfInstruction.next.next.item = offset[1];
	}


	private void createStackEntry(int codeContentSize, int lineNumber) {

		if (codeContentSize == lastStackAddress) {
			return;
		}

		Map<VariableType, Integer> typesInUse = MuseUtils.countVariables(variablesInUse);
		List<VariableType> newTypes = MuseUtils.getNewVariables(typesUsedAtLastStackAddress, typesInUse);
		typesUsedAtLastStackAddress = typesInUse;

		int stackAddress = stackData.size() == 0 ? codeContentSize : codeContentSize - lastStackAddress - 1;
		lastStackAddress = codeContentSize;

		if (newTypes.size() == 0) {
			if (stackAddress < 0) {
				throw new RuntimeException("Line " + lineNumber + ": Offset " + stackAddress + " should not be negative.");
			}
			if (stackAddress <= 63) {
				stackData.add(new byte[] {(byte) stackAddress});

			} else if (stackAddress <= 65535) {
				byte[] bytesToAdd = new byte[3];
				bytesToAdd[0] = (byte)251;
				byte[] address = MuseUtils.intToBytes(stackAddress, 2);
				bytesToAdd[1] = address[0];
				bytesToAdd[2] = address[1];
				stackData.add(bytesToAdd);

			} else {
				throw new RuntimeException("Line " + lineNumber + ": Offset " + stackAddress + " is too high.");
			}
		} else if (newTypes.size() == 1) {
			if (newTypes.get(0) == VariableType.Int || newTypes.get(0) == VariableType.Bool) {
				byte[] address = MuseUtils.intToBytes(stackAddress, 2);
				// 252 -> append 1 variable, 1 -> int type
				stackData.add(new byte[] {(byte) 252, address[0], address[1], (byte) 1});
			} else {
				throw new RuntimeException("Unknown type: " + newTypes.get(0));
			}
		} else if (newTypes.size() == 2) {
			if ((newTypes.get(0) == VariableType.Int || newTypes.get(0) == VariableType.Bool) && (newTypes.get(1) == VariableType.Int || newTypes.get(1) == VariableType.Bool)) {
				byte[] address = MuseUtils.intToBytes(stackAddress, 2);
				// 252 -> append 1 variable, 1 -> int type
				stackData.add(new byte[] {(byte) 253, address[0], address[1], (byte) 1, (byte) 1});
			} else {
				throw new RuntimeException("Unknown type: " + newTypes.get(0));
			}
		} else {
			throw new RuntimeException("Too many new variables " + newTypes.size());
		}
	}
}