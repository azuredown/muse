import org.antlr.v4.runtime.misc.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MuseUtils {

	// Used to make sure we don't go over our 4 byte allotted limit.
	public static void assertNumFitsIn4Bytes(int num) {
		if (num > 65536) {
			throw new RuntimeException("Too many items!");
		} else if (num > 32768) {
			System.out.println("Time to invest in unsigned integers.");
		}
	}


	public static byte[] intToBytes(int value) {

		if (value >= 0) {
			byte[] returnArray = new byte[(int)Math.ceil((Math.log(2 * (value + 1)) / Math.log(2)) / 8)];

			for (int i = 0; i < returnArray.length; i++) {
				returnArray[i] = (byte) (value / ((int)Math.pow(2, (returnArray.length - i - 1) * 8)));
			}

			return returnArray;

		} else {
			byte[] returnArray = intToBytes(-(value + 1));

			for(int i = 0; i < returnArray.length; i++) {
				returnArray[i] = (byte)(~returnArray[i] & 0xff);
			}

			return returnArray;
		}
	}
	public static byte[] intToBytes(int value, int size) {

		if (value >= 0) {

			if (2 * (value + 1) > ((int)Math.pow(2, 8 * size))) {
				throw new RuntimeException("Unable to fit int. Size given: " + size + " Value: " + value);
			}

			byte[] returnArray = new byte[size];

			for (int i = 0; i < returnArray.length; i++) {
				returnArray[i] = (byte) (value / ((int)Math.pow(2, (returnArray.length - i - 1) * 8)));
			}

			return returnArray;

		} else {
			byte[] returnArray = intToBytes(-(value + 1), size);

			for(int i = 0; i < returnArray.length; i++) {
				returnArray[i] = (byte)(~returnArray[i] & 0xff);
			}

			return returnArray;
		}
	}


	public static String fieldName(String classHierarchy, String nameInCode) {
		return classHierarchy + "." + nameInCode;
	}


	public static String multiplyString(String string, int numTimes) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < numTimes; i++) {
			builder.append(string);
		}
		return builder.toString();
	}


	// Can be null. Takes a type signature (like ()V) and returns the return type.
	public static VariableType getReturnType(String typeSignature) {
		String returnTypeAsString = typeSignature.substring(typeSignature.lastIndexOf(")") + 1);
		if (returnTypeAsString.equals("V")) {
			return null;
		} else if (returnTypeAsString.equals("I")) {
			return VariableType.Int;
		}
		throw new RuntimeException("Unknown return type signature: " + typeSignature);
	}


	public static int countIndents(String input) {
		if (input.startsWith("\t")) {
			return countIndents(input.substring(1)) + 1;
		}
		return 0;
	}


	public static String trim(String toStrip) {
		toStrip = leftTrim(toStrip);
		return rightTrim(toStrip);
	}


	private static String leftTrim(String toStrip) {
		while (toStrip.length() != 0 && (toStrip.startsWith(" ") || toStrip.startsWith("\t") || toStrip.startsWith("\r") || toStrip.startsWith("\n"))) {
			toStrip = toStrip.substring(1);
		}
		return toStrip;
	}


	private static String rightTrim(String toStrip) {
		while (toStrip.length() != 0 && (toStrip.endsWith(" ") || toStrip.endsWith("\t") || toStrip.endsWith("\r") || toStrip.endsWith("\n"))) {
			toStrip = toStrip.substring(0, toStrip.length() - 1);
		}
		return toStrip;
	}


	public static boolean isInt(String value) {
		return value.matches("-?\\d+");
	}


	public static int totalBytes(List<byte[]> bytes) {
		int counter = 0;

		for (int i = 0; i < bytes.size(); i++) {
			counter += bytes.get(i).length;
		}

		return counter;
	}


	public static Map<VariableType, Integer> countVariables(Map<String, Pair<Integer, VariableType>> variablesInUse) {
		Map<VariableType, Integer> values = new HashMap<VariableType, Integer>();

		for (Pair<Integer, VariableType> pair : variablesInUse.values()) {
			if (!values.containsKey(pair.b)) {
				values.put(pair.b, 1);
			} else {
				values.put(pair.b, values.get(pair.b) + 1);
			}
		}

		return values;
	}


	public static List<VariableType> getNewVariables(Map<VariableType, Integer> oldVariableMap, Map<VariableType, Integer> newVariableMap) {
		List<VariableType> newVariables = new ArrayList<VariableType>();

		for (VariableType type : newVariableMap.keySet()) {
			if (!oldVariableMap.containsKey(type)) {
				for (int i = 0; i < newVariableMap.get(type); i++) {
					newVariables.add(type);
				}
			} else {
				for (int i = oldVariableMap.get(type); i < newVariableMap.get(type); i++) {
					newVariables.add(type);
				}
			}
		}

		return newVariables;
	}


	public static byte[] positiveIntToBytesFiniteSize(int value, int size) {

		byte[] returnArray = new byte[size];

		if (size > ((int)Math.pow(2, returnArray.length * 8))) {
			throw new RuntimeException("Unable to fit int. Size given: " + size + " Value: " + value);
		}
		for (int i = 0; i < returnArray.length; i++) {
			returnArray[i] = (byte) (value / ((int)Math.pow(2, (returnArray.length - i - 1) * 8)));
		}

		return returnArray;
	}
}
