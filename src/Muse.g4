grammar Muse;

// Note: fragment is sort of like inline

// Parser rules: operates on tokens

program
	:	( assignment_op | goto_op | print_op | label_op | incrementDecrement_op | operationEquals_op | if_op | loop_op |
		lambda_op | dysfunction_op | break_op | else_op | elif_op )* EOF
	;

dysfunction_op
	:	DYSFUNCTION
	;

lambda_op
	:	LAMBDA
	;

assignment_op
	:	ASSIGNMENT
	;

print_op
	:	PRINT
	;

if_op
	:	IF
	;

elif_op
	:	ELIF
	;

else_op
	:	ELSE
	;

label_op
	:	LABEL
	;

goto_op
	:	GOTO
	;

incrementDecrement_op
	:	INCREMENT_DECREMENT
	;

operationEquals_op
	:	OPERATION_EQUALS
	;

loop_op
	:	LOOP
	;

break_op
	:	BREAK
	;

// Lexer rules: create tokens
// When ANTLR runs it converts everything to tokens naively so it's important none of these rules conflict or if they
// do the one we actually want is placed earlier in the list.

// Comments must be first or they may accidentally match variables
COMMENT_MULTILINE
	:   '/*' .*? '*/' -> channel(HIDDEN)
	;

COMMENT_SINGLELINE
	:   '//' ~[\r\n]* -> channel(HIDDEN)
	;

// This must be before RETURN_WORD. Why? ¯\_(ツ)_/¯
BREAK
	:	[\t]* 'break'
	;

OPERATION_EQUALS
	:	NAME [ \n]+ ( '*=' | '/=' | '+=' | '-=' ) [ \n]+ INTEGER
	;

LABEL
	:	[\t]* 'label' [ \n]+ NAME
	;

GOTO
	:	[\t]* 'goto' [ \n]+ NAME
	;

IF
	:	[\t]* 'if' ' '* EXPRESSION ' '* ':'?
	;

ELIF
	:	[\t]* 'elif' ' '* EXPRESSION ' '* ':'?
	;

ELSE
	:	[\t]* 'else' ':'?
	;

LOOP
	:	[\t]* ('do' ' '+)? 'while' ' '+ BOOL_EXPRESSION ' '* ':'?
	;

CONST
	:	'const'
	;

LAMBDA
	:	[\t]* CONST [ \n]+ NAME [ \n]* '<-' [ \n]* 'lambda' [ \n]+ (PARAMETER WS+)* ( PARAMETER | 'void' ) [ \n]* ':'?
	;

ASSIGNMENT
	:	[\t]* NAME [ \n]* '<-' [ \n]* ( DYSFUNCTION | ARITHMETIC_EXPRESSION | BOOL_EXPRESSION )
	;

BOOL
	:	'true' | 'false'
	;

INCREMENT_DECREMENT
	:	'\t'* NAME [ \n]* ( '++' | '--' )
	;

STRING
	:	( '\'' ~('\'')* '\'' ) | ( '"' ~('"')* '"' )
	;

INTEGER
	:	'0' | '-'? [1-9][0-9]*
	;

PRINT
	:	[\t]* 'print' (WS+ ARGUMENT)+
	;

DYSFUNCTION
	:	[\t]* './'? NAME (WS+ ARGUMENT)*
	;

// Don't put this in the WHITESPACE rule because that causes tabs to be ignored completely
DANGLING_INDENTS
	:	[\t]+ -> skip
	;

EXPRESSION
	:	BOOL_EXPRESSION | ARITHMETIC_EXPRESSION
	;

EXPRESSION2
	:	(EXPRESSION_CLAUSE WS+ OPERATOR_SYMBOL WS+)* EXPRESSION_CLAUSE
	;

fragment
EXPRESSION_CLAUSE
	:	( '(' EXPRESSION2 ')' ) | BOOL_EXPRESSION | ARITHMETIC_EXPRESSION | (NAME (WS+ EXPRESSION_CLAUSE)*)
	;

// This rule is just so I can have tabs on blank lines (because some text exitors do not trim the on save)
LEADING_TABS
	:	'\n' '\t'+ '\n' -> skip
	;

WHITESPACE
	:   ([ \r\n]+) -> skip
	;

fragment
ARGUMENT
	:	( STRING | INTEGER | NAME | BOOL | EXPRESSION )
	;

fragment
NAME
	:	NONWHITESPACE_CHAR* [a-zA-Z] NONWHITESPACE_CHAR*
	;

fragment
PARAMETER
	:	( 'int' | 'bool' ) [ \n]+ NAME
	;

// White space that can be used in commands. Unlike the other whitespace which is jsut used to sepparate things.
fragment
WS
	:	( ' ' | '\\n' )
	;

fragment
MULTI_BOOL_EXPRESSION
	:	( SINGLE_BOOL_EXPRESSION | ( LEFT_BRACKET BOOL_EXPRESSION RIGHT_BRACKET )) WS*
		( ' and ' | ' or ' )
		WS* ( SINGLE_BOOL_EXPRESSION | ( LEFT_BRACKET BOOL_EXPRESSION RIGHT_BRACKET ))
	;

fragment
BOOL_EXPRESSION
	:	MULTI_BOOL_EXPRESSION | SINGLE_BOOL_EXPRESSION
	;

fragment
ARITHMETIC_BOOL
	:	( NAME [ \n]* ( '<' | '>' | '=' | '>=' | '<=' ) [ \n]* ARITHMETIC_EXPRESSION ) |
		( ARITHMETIC_EXPRESSION [ \n]* ( '<' | '>' | '=' | '>=' | '<=' ) [ \n]* NAME )
	;

fragment
ARITHMETIC_EXPRESSION
	:	ARITHMETIC_OPERATION* ( INTEGER | NAME )
	;

fragment
ARITHMETIC_OPERATION
	:	( INTEGER | NAME) WS* OPERATOR_SYMBOL WS*
	;

// 28 is left bracket and 29 is right bracket
fragment
NONWHITESPACE_CHAR
	:	~[ \r\n+\-\u0028\u0029]
	;

fragment
SINGLE_BOOL_EXPRESSION
	:	BOOL | NAME | ARITHMETIC_BOOL
	;

fragment
LEFT_BRACKET
	:	' '* '(' ' '*
	;

fragment
RIGHT_BRACKET
	:	' '* ')' ' '*
	;

fragment
OPERATOR_SYMBOL
	:	( '+' | '-' | '*' | '/' | '%' | ' and ' | ' or ' | '=' | '>=' | '<=' | '>' | '<' )
	;


//UNICODE
//	:   '\u0000'..'\u00FF'
//	;