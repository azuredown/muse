public enum ConstantType {
	Constant, // ID 1
	ClassName, // ID 7
	String, // ID 8
	FieldReference, // ID 9
	MethodReference, // ID 10
}