import java.util.List;

public class IndentInformation {

	// For while and do while loops as we may not have a reference to the first jump otherwise.
	public LinkedListNode<Byte> startOfIndentBlock;

	// Every one of these jumps must be redirected to point to the end of the indent. For if statements with multiple
	// conditions.
	public List<LinkedListNode<Byte>> jumpLocations;

	// Every one of these gotos must be redirected to the end. For elif statements.
	public List<LinkedListNode<Byte>> gotos;

	public IndentReason indentReason;

	// For do while so we can evaluate the condition at the end instead of the beginning. Can be null.
	public String boolCondition;

	public IndentInformation(LinkedListNode<Byte> startOfIndentBlock, List<LinkedListNode<Byte>> jumpLocations,
							 IndentReason indentReason, String boolCondition, List<LinkedListNode<Byte>> gotos) {
		this.startOfIndentBlock = startOfIndentBlock;
		this.jumpLocations = jumpLocations;
		this.indentReason = indentReason;
		this.boolCondition = boolCondition;
		this.gotos = gotos;
	}
}