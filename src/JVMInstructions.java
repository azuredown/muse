import java.util.Map;

// Static functions that allow us to easily use the JVM assembly instructions.
public class JVMInstructions {

	// Gets a static function address
	public static void getStatic(MyLinkedList<Byte> methodBytes, Map<ConstantType, Map<String, Integer>> constantIndices, String fieldName) {
		int location = constantIndices.get(ConstantType.FieldReference).get(fieldName);
		methodBytes.add((byte) -78);
		methodBytes.add((byte) (location / 256));
		methodBytes.add((byte) location);
	}

	// Calls a function
	public static void invokeVirtual(MyLinkedList<Byte> methodBytes, Map<ConstantType, Map<String, Integer>> constantIndices, String thingToCall) {
		methodBytes.add((byte) -74);
		int location = constantIndices.get(ConstantType.MethodReference).get(thingToCall);
		methodBytes.add((byte)(location / 256));
		methodBytes.add((byte)location);
	}
	public static void invokeSpecial(MyLinkedList<Byte> methodBytes, Map<ConstantType, Map<String, Integer>> constantIndices, String thingToCall) {
		methodBytes.add((byte) 183);
		int location = constantIndices.get(ConstantType.MethodReference).get(thingToCall);
		methodBytes.add((byte)(location / 256));
		methodBytes.add((byte)location);
	}


	public static void aload(MyLinkedList<Byte> methodBytes, int index) {
		switch (index) {
			case 0:
				methodBytes.add((byte) 42);
				break;
			case 1:
				methodBytes.add((byte) 43);
				break;
			case 2:
				methodBytes.add((byte) 44);
				break;
			case 3:
				methodBytes.add((byte) 45);
				break;
			default:
				throw new RuntimeException("Unknown address index: " + index);
		}
	}


	// Assigns a variable to a global variable
	public static void putField(MyLinkedList<Byte> codeContent, int fieldToSaveTo) {
		byte[] bytesToWrite = MuseUtils.intToBytes(fieldToSaveTo, 2);
		codeContent.add((byte)181);
		codeContent.add(bytesToWrite[0]);
		codeContent.add(bytesToWrite[1]);
	}


	public static void getField(MyLinkedList<Byte> codeContent, int fieldIndex) {
		byte[] bytesToWrite = MuseUtils.intToBytes(fieldIndex, 2);
		codeContent.add((byte)180);
		codeContent.add(bytesToWrite[0]);
		codeContent.add(bytesToWrite[1]);
	}


	// Duplicates the item on the stack
	public static void dup(MyLinkedList<Byte> methodBytes) {
		methodBytes.add((byte) 89);
	}


	public static void newCommand(MyLinkedList<Byte> methodBytes, Map<ConstantType, Map<String, Integer>> constantIndices, String className) {
		methodBytes.add((byte) 187);
		byte[] indexInPool = MuseUtils.intToBytes(constantIndices.get(ConstantType.ClassName).get(className), 2);
		methodBytes.add(indexInPool[0]);
		methodBytes.add(indexInPool[1]);
	}


	// iinc command
	public static void incrementByConstant(MyLinkedList<Byte> methodBytes, int positionOfVariable, int valueToIncrementBy) {
		if (valueToIncrementBy <= 127 && valueToIncrementBy >= -128) {
			methodBytes.add((byte) 132);
			methodBytes.add(MuseUtils.intToBytes(positionOfVariable, 1)[0]);
			methodBytes.add(MuseUtils.intToBytes(valueToIncrementBy, 1)[0]);
		}

		// This is wide iinc or iinc_w
		else if (valueToIncrementBy <= 32767 && valueToIncrementBy >= -32768) {
			methodBytes.add((byte) 196);
			methodBytes.add((byte) 132);
			byte[] bytes = MuseUtils.intToBytes(positionOfVariable, 2);
			methodBytes.add(bytes[0]);
			methodBytes.add(bytes[1]);
			bytes = MuseUtils.intToBytes(valueToIncrementBy, 2);
			methodBytes.add(bytes[0]);
			methodBytes.add(bytes[1]);
		}

		else {
			throw new RuntimeException("Incrementing by invalid number: " + valueToIncrementBy);
		}
	}

	// Calls a function
	public static void loadConstant(MyLinkedList<Byte> methodBytes, int positionInConstantPool) {
		methodBytes.add((byte) 18);
		if (positionInConstantPool > 255) {
			throw new RuntimeException("Signed integer " + positionInConstantPool + " is too big!");
		}
		methodBytes.add((byte)positionInConstantPool);
	}


	// idiv
	public static void divideInt(MyLinkedList<Byte> methodBytes) {
		methodBytes.add((byte) 108);
	}
	// imul
	public static void multiplyInt(MyLinkedList<Byte> methodBytes) {
		methodBytes.add((byte) 104);
	}


	// Adds a temporary goto statement. The temporary value will be replaced with a permanent version when all
	// labels have been processed. Uses the 2 byte for goto. There's also a 4 byte version but that shouldn't be
	// necessary.
	public static void gotoTemporary(MyLinkedList<Byte> methodBytes) {
		methodBytes.add((byte) -89);
		methodBytes.add((byte)0);
		methodBytes.add((byte)0);
	}
	// The JVM specifies the goto as an offset from the current location, not the absolute position
	public static void gotoPermanent(MyLinkedList<Byte> methodBytes, LinkedListNode<Byte> gotoPosition, int labelPosition) {
		byte[] gotoOffset = MuseUtils.intToBytes(labelPosition, 2);
		gotoPosition = gotoPosition.next;
		gotoPosition.item = gotoOffset[0];
		gotoPosition = gotoPosition.next;
		gotoPosition.item = gotoOffset[1];
	}
	public static void addGotoPermanent(MyLinkedList<Byte> methodBytes, int positionGoingTo) {
		byte[] goingToOffset = MuseUtils.intToBytes(positionGoingTo, 2);
		methodBytes.add((byte) -89);
		methodBytes.add(goingToOffset[0]);
		methodBytes.add(goingToOffset[1]);
	}


	// Creates 3 bytes (empty instruction, 2 bytes for address) and returns the LinkedListNode of the start of the
	// command
	public static LinkedListNode<Byte> addTemporary(MyLinkedList<Byte> methodBytes, byte command) {
		methodBytes.add(command);
		LinkedListNode<Byte> instructionIndex = methodBytes.last;
		methodBytes.add((byte) 0);
		methodBytes.add((byte) 0);
		return instructionIndex;
	}
	public static void addPermanent(MyLinkedList<Byte> methodBytes, byte command, int offset) {
		methodBytes.add(command);
		byte[] gotoOffset = MuseUtils.intToBytes(offset, 2);
		methodBytes.add(gotoOffset[0]);
		methodBytes.add(gotoOffset[1]);
	}


	public static void ifTemporary(MyLinkedList<Byte> methodBytes) {
		methodBytes.add((byte) 153); // If equal
		methodBytes.add((byte)0);
		methodBytes.add((byte)0);
	}
	// The JVM specifies the goto as an offset from the current location, not the absolute position
	public static void ifPermanent(MyLinkedList<Byte> methodBytes, LinkedListNode<Byte> commandInstruction, int offset) {
		byte[] gotoOffset = MuseUtils.intToBytes(offset, 2);

		commandInstruction = commandInstruction.next;
		commandInstruction.item = gotoOffset[0];
		commandInstruction = commandInstruction.next;
		commandInstruction.item = gotoOffset[1];
	}
	//public static void addIfPermanent(List<Byte> methodBytes, int offset) {
	//	methodBytes.add((byte) 153); // If equal
	//	byte[] offsetBytes = MuseUtils.intToBytes(offset, 2);
	//	methodBytes.add(offsetBytes[0]);
	//	methodBytes.add(offsetBytes[1]);
	//}
	// If condition is satisfied go back to top of loop
	public static void doWhilePermanent(MyLinkedList<Byte> methodBytes, int offset) {
		methodBytes.add((byte) 154); // If not equal
		byte[] gotoOffset = MuseUtils.intToBytes(offset, 2);
		methodBytes.add(gotoOffset[0]);
		methodBytes.add(gotoOffset[1]);
	}

	// Gets an integer constant
	public static void pushInt(MyLinkedList<Byte> methodBytes, int value) {
		// -1 - 5 are hard coded into the JVM (command iConst)
		switch (value) {
			case -1:
				methodBytes.add((byte)2);
				return;
			case 0:
				methodBytes.add((byte)3);
				return;
			case 1:
				methodBytes.add((byte)4);
				return;
			case 2:
				methodBytes.add((byte)5);
				return;
			case 3:
				methodBytes.add((byte)6);
				return;
			case 4:
				methodBytes.add((byte)7);
				return;
			case 5:
				methodBytes.add((byte)8);
				return;
		}

		// -128 - 127 are covered by bipush
		if (value >= -128 && value <= 127) {
			methodBytes.add((byte)16);
			methodBytes.add((byte)value);
			return;
		}

		// -32768 - 32767 are covered by sipush
		if (value >= -32768 && value <= 32767) {
			methodBytes.add((byte)17);
			byte[] bytes = MuseUtils.intToBytes(value);
			for (int i = 0; i < bytes.length; i++) {
				methodBytes.add(bytes[i]);
			}
			return;
		}
	}


	// Loads an integer from the method's data array
	public static void iLoad(MyLinkedList<Byte> methodBytes, int value) {
		// 0 - 4 are hard coded into the JVM
		switch (value) {
			case 0:
				methodBytes.add((byte)26);
				break;
			case 1:
				methodBytes.add((byte)27);
				break;
			case 2:
				methodBytes.add((byte)28);
				break;
			case 3:
				methodBytes.add((byte)29);
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + value);
		}
	}


	// Stores an integer into the method's data array
	public static void iStore(MyLinkedList<Byte> methodBytes, int value) {
		// 0 - 4 are hard coded into the JVM
		switch (value) {
			case 0:
				methodBytes.add((byte)59);
				break;
			case 1:
				methodBytes.add((byte)60);
				break;
			case 2:
				methodBytes.add((byte)61);
				break;
			case 3:
				methodBytes.add((byte)62);
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + value);
		}
	}


	public static void putStatic(MyLinkedList<Byte> methodBytes, int indexOfStaticValue) {
		methodBytes.add((byte)179);
		byte[] bytesToAdd = MuseUtils.intToBytes(indexOfStaticValue, 2);
		methodBytes.add(bytesToAdd[0]);
		methodBytes.add(bytesToAdd[1]);
	}
}
