import java.util.List;

public class FunctionBytes {

	public List<Byte> exceptions;
	public List<Byte> stackData;
	public MyLinkedList<Byte> code;
	public int variablesInUse, stackSize;
	public String name;
	public String typeSignature;

	public FunctionBytes(String name, List<Byte> exceptions, List<Byte> stackData, MyLinkedList<Byte> code, int variablesInUse, int stackSize, String typeSignature) {
		this.exceptions = exceptions;
		this.stackData = stackData;
		this.code = code;
		this.variablesInUse = variablesInUse;
		this.stackSize = stackSize;
		this.name = name;
		this.typeSignature = typeSignature;
	}
}

	