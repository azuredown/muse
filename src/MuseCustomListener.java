
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.*;

public class MuseCustomListener implements MuseListener {

	int currentContainerIndentLevel = 0;

	private ContainerBlock currentContainer;

	private Stack<List<String>> stackOfListsOfTokens = new Stack<List<String>>();

	private int lineNum;

	private ClassFileSharedData sharedData;


	public MuseCustomListener(ClassFileSharedData sharedData, ContainerBlock startingContainer, int lineNum) {

		this.sharedData = sharedData;
		this.currentContainer = startingContainer;
		this.lineNum = lineNum;
	}


	public void enterProgram(MuseParser.ProgramContext ctx) {
	}
	public void exitProgram(MuseParser.ProgramContext ctx) {
		while (currentContainerIndentLevel >= 0) {
			currentContainerIndentLevel--;

			endCurrentContainer(lineNum + 1);

			currentContainer = currentContainer.parentContainer;
		}
	}

	public void enterDysfunction_op(MuseParser.Dysfunction_opContext ctx) {
	}
	public void exitDysfunction_op(MuseParser.Dysfunction_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());

		String functionCall = MuseUtils.trim(stackOfListsOfTokens.peek().get(0));

		String functionName;
		List<String> arguments = new ArrayList<String>();

		if (functionCall.contains(" ")) {
			functionName = functionCall.substring(0, functionCall.indexOf(" "));

			int counter = 0;
			while (true) {
				counter ++;
				if (counter > 100) {
					throw new RuntimeException("Line " + ctx.start.getLine() + ": Infinite loop detected.");
				}
				functionCall = MuseUtils.trim(functionCall.substring(functionCall.indexOf(" ") + 1));

				if (functionCall.contains(" ")) {
					arguments.add(functionCall.substring(0, functionCall.indexOf(" ")));
				} else {
					arguments.add(functionCall);
					break;
				}
			}
		} else {
			functionName = functionCall;
		}

		currentContainer.notifyFunctionCall(ctx.start.getLine(), functionName, arguments, 0, false);
	}


	private void endCurrentContainer(int lineNumber) {
		currentContainer.notifyDone(lineNumber);
	}


	private void checkIndentLevel(List<String> tokens, int lineNumber) {
		if (tokens.size() == 0) {
			throw new RuntimeException("Line " + lineNumber + ": no tokens found.");
		}
		int indentNum = MuseUtils.countIndents(tokens.get(0));
		while (indentNum < currentContainerIndentLevel) {
			currentContainerIndentLevel--;
			endCurrentContainer(lineNumber);
			currentContainer = currentContainer.parentContainer;
		}
		tokens.set(0, tokens.get(0).substring(currentContainerIndentLevel));
	}

	public void enterLambda_op(MuseParser.Lambda_opContext ctx) {
	}
	public void exitLambda_op(MuseParser.Lambda_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainerIndentLevel++;

		String command = stackOfListsOfTokens.peek().get(0);
		String name = command.substring(command.indexOf("const") + 5, command.indexOf("<-"));
		name = MuseUtils.trim(name);

		ContainerVariableInfo info = sharedData.containerSignatures.get(name);

		ContainerBlock newContainer = new ContainerBlock(name, sharedData, info, ctx.stop.getLine(), false);
		newContainer.parentContainer = currentContainer;
		currentContainer.childContainers.add(newContainer);
		currentContainer = newContainer;
	}

	// Hard coded methods to handle entering/exiting each rule. Not sure why ANTLR uses so much hard coding.
	public void enterPrint_op(MuseParser.Print_opContext ctx) {
	}
	public void exitPrint_op(MuseParser.Print_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyPrint(ctx, stackOfListsOfTokens.peek());
	}

	public void enterIf_op(MuseParser.If_opContext ctx) {
	}
	public void exitIf_op(MuseParser.If_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyIf(ctx, stackOfListsOfTokens.peek());
	}

	public void enterElif_op(MuseParser.Elif_opContext ctx) {
	}
	public void exitElif_op(MuseParser.Elif_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyElif(ctx.start.getLine(), stackOfListsOfTokens.peek().get(0));
	}

	public void enterElse_op(MuseParser.Else_opContext ctx) {
	}
	public void exitElse_op(MuseParser.Else_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyElse(ctx.start.getLine(), stackOfListsOfTokens.peek().get(0));
	}

	public void enterLabel_op(MuseParser.Label_opContext ctx) {
	}
	public void exitLabel_op(MuseParser.Label_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyLabel(ctx, stackOfListsOfTokens.peek());
	}

	public void enterGoto_op(MuseParser.Goto_opContext ctx) {
	}
	public void exitGoto_op(MuseParser.Goto_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyGoto(ctx, stackOfListsOfTokens.peek());
	}

	public void enterIncrementDecrement_op(MuseParser.IncrementDecrement_opContext ctx) {
	}
	public void exitIncrementDecrement_op(MuseParser.IncrementDecrement_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyIncrementDecrement(ctx, stackOfListsOfTokens.peek());
	}

	public void enterOperationEquals_op(MuseParser.OperationEquals_opContext ctx) {
	}
	public void exitOperationEquals_op(MuseParser.OperationEquals_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyOperationEquals(ctx.start.getLine(), stackOfListsOfTokens.peek());
	}

	public void enterLoop_op(MuseParser.Loop_opContext ctx) {
	}
	public void exitLoop_op(MuseParser.Loop_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyLoop(ctx, stackOfListsOfTokens.peek());
	}

	public void enterBreak_op(MuseParser.Break_opContext ctx) {
	}
	public void exitBreak_op(MuseParser.Break_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyBreak(stackOfListsOfTokens.peek().get(0), ctx.start.getLine());
	}

	public void enterAssignment_op(MuseParser.Assignment_opContext ctx) {
	}
	public void exitAssignment_op(MuseParser.Assignment_opContext ctx) {
		checkIndentLevel(stackOfListsOfTokens.peek(), ctx.start.getLine());
		currentContainer.notifyAssignment(ctx, stackOfListsOfTokens.peek());
	}


	public void enterEveryRule(ParserRuleContext ctx) {
		stackOfListsOfTokens.push(new ArrayList<String>());
	}
	public void exitEveryRule(ParserRuleContext ctx) {
		stackOfListsOfTokens.pop();
	}


	// Visit a leaf node in the tree if there are no syntax errors
	public void visitTerminal(TerminalNode node) {
		stackOfListsOfTokens.get(stackOfListsOfTokens.size() - 1).add(node.toString());
	}


	// Visit a leaf node in the tree if there is a syntax error
	public void visitErrorNode(ErrorNode node) {
		throw new RuntimeException("Syntax error when trying to visit node: " + node.toString());
	}
}