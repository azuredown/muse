rem Just calling antlr4 is insufficient to rebuild grammar. We have to compile the java files as well.
call antlr4 Muse.g4
call javac *.java