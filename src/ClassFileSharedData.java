import org.antlr.v4.runtime.misc.Triple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassFileSharedData {

	// Name -> Field reference, type, isStatic
	public Map<String, Triple<Integer, VariableType, Boolean>> globalVariables = new HashMap<String, Triple<Integer, VariableType, Boolean>>();

	// Constant Type -> Name -> Integer
	public Map<ConstantType, Map<String, Integer>> constantIndices = new HashMap<ConstantType, Map<String, Integer>>();

	public List<byte[]> constantPool = new ArrayList<byte[]>();

	// Function name -> function info
	public Map<String, ContainerVariableInfo> containerSignatures = new HashMap<String, ContainerVariableInfo>();

	public String className;

	// Variable name -> type as string
	public Map<String, String> objectTypes = new HashMap<String, String>();

	public ClassFileSharedData(String name) {
		this.className = name;
	}

	public boolean functionDefined(String functionCall) {
		if (functionCall.contains(" ")) {
			return containerSignatures.containsKey(functionCall.substring(0, functionCall.indexOf(" ")));
		} else {
			return containerSignatures.containsKey(functionCall);
		}
	}

	public VariableType returnType(String functionName) {
		return containerSignatures.get(functionName).returnType;
	}

	public boolean hasStaticVariable(String name) {
		return globalVariables.containsKey(name);
	}

	public Triple<Integer, VariableType, Boolean> staticVarType(String name) {
		return globalVariables.get(name);
	}

	public void addStaticVar(String classHierarchy, String name, VariableType type, boolean isStatic) {
		globalVariables.put(name, new Triple<Integer, VariableType, Boolean>(constantIndices.get(ConstantType.FieldReference).get(MuseUtils.fieldName(classHierarchy, name)), type, isStatic));
	}
}