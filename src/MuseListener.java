// Generated from Muse.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MuseParser}.
 */
public interface MuseListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MuseParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MuseParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MuseParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#dysfunction_op}.
	 * @param ctx the parse tree
	 */
	void enterDysfunction_op(MuseParser.Dysfunction_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#dysfunction_op}.
	 * @param ctx the parse tree
	 */
	void exitDysfunction_op(MuseParser.Dysfunction_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#lambda_op}.
	 * @param ctx the parse tree
	 */
	void enterLambda_op(MuseParser.Lambda_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#lambda_op}.
	 * @param ctx the parse tree
	 */
	void exitLambda_op(MuseParser.Lambda_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#assignment_op}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_op(MuseParser.Assignment_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#assignment_op}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_op(MuseParser.Assignment_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#print_op}.
	 * @param ctx the parse tree
	 */
	void enterPrint_op(MuseParser.Print_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#print_op}.
	 * @param ctx the parse tree
	 */
	void exitPrint_op(MuseParser.Print_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#if_op}.
	 * @param ctx the parse tree
	 */
	void enterIf_op(MuseParser.If_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#if_op}.
	 * @param ctx the parse tree
	 */
	void exitIf_op(MuseParser.If_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#elif_op}.
	 * @param ctx the parse tree
	 */
	void enterElif_op(MuseParser.Elif_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#elif_op}.
	 * @param ctx the parse tree
	 */
	void exitElif_op(MuseParser.Elif_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#else_op}.
	 * @param ctx the parse tree
	 */
	void enterElse_op(MuseParser.Else_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#else_op}.
	 * @param ctx the parse tree
	 */
	void exitElse_op(MuseParser.Else_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#label_op}.
	 * @param ctx the parse tree
	 */
	void enterLabel_op(MuseParser.Label_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#label_op}.
	 * @param ctx the parse tree
	 */
	void exitLabel_op(MuseParser.Label_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#goto_op}.
	 * @param ctx the parse tree
	 */
	void enterGoto_op(MuseParser.Goto_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#goto_op}.
	 * @param ctx the parse tree
	 */
	void exitGoto_op(MuseParser.Goto_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#incrementDecrement_op}.
	 * @param ctx the parse tree
	 */
	void enterIncrementDecrement_op(MuseParser.IncrementDecrement_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#incrementDecrement_op}.
	 * @param ctx the parse tree
	 */
	void exitIncrementDecrement_op(MuseParser.IncrementDecrement_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#operationEquals_op}.
	 * @param ctx the parse tree
	 */
	void enterOperationEquals_op(MuseParser.OperationEquals_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#operationEquals_op}.
	 * @param ctx the parse tree
	 */
	void exitOperationEquals_op(MuseParser.OperationEquals_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#loop_op}.
	 * @param ctx the parse tree
	 */
	void enterLoop_op(MuseParser.Loop_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#loop_op}.
	 * @param ctx the parse tree
	 */
	void exitLoop_op(MuseParser.Loop_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#break_op}.
	 * @param ctx the parse tree
	 */
	void enterBreak_op(MuseParser.Break_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#break_op}.
	 * @param ctx the parse tree
	 */
	void exitBreak_op(MuseParser.Break_opContext ctx);
}