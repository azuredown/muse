import org.antlr.v4.runtime.misc.Pair;

import java.util.List;

public class ContainerVariableInfo {

	public VariableType returnType;
	public List<Pair<VariableType, String>> parameters;

	public ContainerVariableInfo(VariableType returnType, List<Pair<VariableType, String>> parameters) {
		this.parameters = parameters;
		this.returnType = returnType;
	}

	public String makeTypeSignature() {

		StringBuilder returnString = new StringBuilder("(");

		for (int i = 0; i < parameters.size(); i++) {
			returnString.append(typeToStringRepresentation(parameters.get(i).a, ""));
		}

		returnString.append(")");
		returnString.append(typeToStringRepresentation(returnType, ""));

		return returnString.toString();
	}

	public static String typeToStringRepresentation(VariableType type, String nameIfObject) {
		if (type == null) {
			return "V";
		} else if (type == VariableType.Int || type == VariableType.Bool) {
			return "I";
		} else if (type == VariableType.Object) {
			return "L" + nameIfObject + ";";
		} else if (type == VariableType.StringArray) {
			return "[Ljava/lang/String;";
		} else {
			throw new RuntimeException("Unknown return type: " + type);
		}
	}
}