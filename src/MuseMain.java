
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.*;

import java.io.*;
import java.util.*;
import java.util.List;

public class MuseMain {

	private static boolean errorsInsteadOfWarnings = false;

	public static void main(String[] args) {

		List<String> newArgs = new ArrayList<String>(Arrays.asList(args));

		boolean compileOnly = false;
		if (newArgs.contains("-c")) {
			compileOnly = true;
			newArgs.remove("-c");
		}

		if (newArgs.contains("-e")) {
			errorsInsteadOfWarnings = true;
			newArgs.remove("-e");
		}

		// Not exactly sure why we need an argument in toArray()
		args = newArgs.toArray(args);

		if (newArgs.size() != 1) {
			System.out.println("Usage: java MuseMain [-e] INPUT_FILE [args...]");
			System.out.println("\tjava MuseMain -c [-e] INPUT_FILE");
			System.exit(1);
		}

		if (!args[0].endsWith(".muse")) {
			args[0] += ".muse";
		}

		List<String> fileLines = new ArrayList<String>();

		try {
			File file = new File(args[0]);
			Scanner reader = new Scanner(file);
			while (reader.hasNextLine()) {
				fileLines.add(reader.nextLine());
			}
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("Could not find file.");
			return;
		}

		String mainClassName = args[0].contains(".")
				? args[0].substring(0, args[0].lastIndexOf("."))
				: args[0];

		if (mainClassName.startsWith("$")) {
			throw new RuntimeException("Class name can not begin with $");
		}

		List<Pair<VariableType, String>> arguments = new ArrayList<Pair<VariableType, String>>();
		arguments.add(new Pair<VariableType, String>(VariableType.StringArray, "argv"));
		ContainerVariableInfo initialInfo = new ContainerVariableInfo(null, arguments);
		compile(mainClassName, fileLines, fileLines.size(), true, 0, fileLines.size(), 0, initialInfo);

		if (compileOnly) {
			return;
		}

		try {
			Runtime rt = Runtime.getRuntime();
			String[] commands = new String[newArgs.size() + 1];
			commands[0] = "java";
			commands[1] = mainClassName;
			for (int i = 1; i < newArgs.size(); i++) {
				commands[i + 1] = newArgs.get(i);
			}
			Process proc = rt.exec(commands);

			BufferedReader stdInput = new BufferedReader(new
					InputStreamReader(proc.getInputStream()));

			BufferedReader stdError = new BufferedReader(new
					InputStreamReader(proc.getErrorStream()));

			String s = null;
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}

			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}
		} catch (IOException e) {
			System.out.println("IO Exception");
		}
	}

	private static void compile(String className, List<String> fileLines, int lineNum, boolean firstPass, int startLineNum, int endLineNum, int numIndentsToIgnore, ContainerVariableInfo variableInfo) {

		StringBuilder fileAsString = new StringBuilder();
		for (int i = startLineNum; i < endLineNum; i++) {
			if ((numIndentsToIgnore == 0 || !fileLines.get(i).startsWith(MuseUtils.multiplyString("\t", numIndentsToIgnore))) || fileLines.get(i).length() == 0 ) {
				fileAsString.append(fileLines.get(i)).append("\n");
			} else {
				fileAsString.append(fileLines.get(i).substring(numIndentsToIgnore)).append("\n");
			}
		}

		MuseLexer lexer = new MuseLexer(CharStreams.fromString(fileAsString.toString()));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		MuseParser parser = new MuseParser(tokens);
		ParseTreeWalker walker = new ParseTreeWalker();

		ClassFileSharedData sharedData = new ClassFileSharedData(className);

		MuseConstLoader constLoader = new MuseConstLoader(sharedData);
		walker.walk(constLoader, parser.program());

		ContainerBlock startingBlock;
		if (firstPass) {
			startingBlock = new ContainerBlock("main", sharedData, variableInfo, lineNum, false);
		} else {
			startingBlock = new ContainerBlock("<init>", sharedData, variableInfo, lineNum, true);
			startingBlock.callSuper("java/lang/Object");

			for (int i = 0; i < variableInfo.parameters.size(); i++) {
				startingBlock.assignValueToGlobalVariable(variableInfo.parameters.get(i).b);
			}
		}

		// For writing static info. <clinit> is a reserved work in the JVM. Currently not used.
		//ContainerBlock clinitBlock = new ContainerBlock("<clinit>", sharedData,
		//		"()V", null, parameterInfo, lineNum, objectTypes);
		//clinitBlock.notifyDone(0);

		lexer = new MuseLexer(CharStreams.fromString(fileAsString.toString()));
		tokens = new CommonTokenStream(lexer);
		parser = new MuseParser(tokens);
		walker = new ParseTreeWalker();

		MuseCustomListener listener = new MuseCustomListener(sharedData, startingBlock, lineNum);

		if (!firstPass && !errorsInsteadOfWarnings) {
			try {
				// Note that program (in .program()) is a name of a rule. It's hardcoded in oddly enough.
				walker.walk(listener, parser.program());
			} catch (RuntimeException e) {
				System.out.println("Unable to create container " + className + " due to compilation error.");
				return;
			}
		} else {
			// Note that program (in .program()) is a name of a rule. It's hardcoded in oddly enough.
			walker.walk(listener, parser.program());
		}

		// Constants we will need. Can't seem to move these down. It shouldn't matter though.
		ConstantPoolManager.createClassNameConstant(sharedData.constantPool, "java/lang/Object", sharedData.constantIndices);
		ConstantPoolManager.createClassNameConstant(sharedData.constantPool, className, sharedData.constantIndices);
		ConstantPoolManager.writeConstantValue(sharedData.constantPool, "Code", sharedData.constantIndices);

		// Crude way of disabling functions. TODO: improve this.
		List<ContainerBlock> subContainers = startingBlock.childContainers;
		startingBlock.childContainers = new ArrayList<ContainerBlock>();

		ClassFileWriter.write(className, startingBlock, sharedData.constantPool, sharedData.constantIndices, sharedData.globalVariables, sharedData.objectTypes, firstPass);

		if (firstPass) {

			Set<String> previousNames = new HashSet<String>();
			previousNames.add(className);

			for (int i = 0; i < subContainers.size(); i++) {
				if (subContainers.get(i).variableInfo.returnType == null) {

					if (previousNames.contains(subContainers.get(i).name)) {
						throw new RuntimeException("Trying to create a class with name " + subContainers.get(i).name + " which is already in use.");
					}

					compile(subContainers.get(i).name, fileLines, lineNum, false,
							subContainers.get(i).startLine, subContainers.get(i).endLine,
							1, subContainers.get(i).variableInfo);
				}
			}
		}
	}
}
