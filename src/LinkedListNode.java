
public class LinkedListNode<T> {
	public LinkedListNode<T> next;
	public T item;

	// Allows us to point to an instruction that doesn't actually exist yet
	public boolean temporaryNode = false;

	public LinkedListNode() {
	}

	public LinkedListNode(T item) {
		this.item = item;
	}
}