import java.util.*;

// Responsible for adding stuff to the constant pool
public class ConstantPoolManager {

	public static void importPrintlnString(ClassFileSharedData sharedData) {
		createClassNameConstant(sharedData, "java/lang/System");
		createClassNameConstant(sharedData, "java/io/PrintStream");

		// Create the links
		// L means class as type, D means double as type, and I means int as type
		createFieldReferenceConstant(sharedData, "java/lang/System", "out",
				"Ljava/io/PrintStream;");
		createMethodReferenceConstant(sharedData, "java/io/PrintStream", "println",
				"(Ljava/lang/String;)V");
	}
	public static void importPrintlnInt(ClassFileSharedData sharedData) {
		createClassNameConstant(sharedData, "java/lang/System");
		createClassNameConstant(sharedData, "java/io/PrintStream");

		// Create the links
		// L means class as type, D means double as type, and I means int as type
		createFieldReferenceConstant(sharedData, "java/lang/System", "out",
				"Ljava/io/PrintStream;");
		createMethodReferenceConstant(sharedData, "java/io/PrintStream", "println",
				"(I)V");
	}
	public static void importPrintlnBool(ClassFileSharedData sharedData) {
		createClassNameConstant(sharedData, "java/lang/System");
		createClassNameConstant(sharedData, "java/io/PrintStream");

		// Create the links
		// L means class as type, D means double as type, and I means int as type
		createFieldReferenceConstant(sharedData, "java/lang/System", "out",
				"Ljava/io/PrintStream;");
		createMethodReferenceConstant(sharedData, "java/io/PrintStream", "println",
				"(Z)V");
	}


	// Creates the name of the class name in UTF8 (id 1) and a pointer to it (id 7). Why do we need a pointer to
	// something right next to us? No idea.
	public static void createClassNameConstant(ClassFileSharedData sharedData, String toWrite) {
		createClassNameConstant(sharedData.constantPool, toWrite, sharedData.constantIndices);
	}
	public static void createClassNameConstant(List<byte[]> constantPool, String toWrite, Map<ConstantType, Map<String, Integer>> constantIndices) {
		if (alreadyContainsReference(constantIndices, ConstantType.ClassName, toWrite)) {
			return;
		}
		// The + 2 indicates the index of the constant value we're about to write. The 2 is because we write 2 items
		// to the pool: the pointer and the constant value.
		constantPool.add(new byte[]{ 7, (byte)((constantPool.size() + 2) / 256), (byte)(constantPool.size() + 2)});
		addValueToConstantReferences(constantIndices, ConstantType.ClassName, toWrite, constantPool.size());

		writeConstantValue(constantPool, toWrite);
	}
	// String values have id 8.
	public static int createStringConstant(List<byte[]> constantPool, String toWrite, Map<ConstantType, Map<String, Integer>> constantIndices) {
		if (alreadyContainsReference(constantIndices, ConstantType.String, toWrite)) {
			return constantIndices.get(ConstantType.String).get(toWrite);
		}
		// The + 2 indicates the index of the constant value we're about to write. The 2 is because we write 2 items
		// to the pool: the pointer and the constant value.
		constantPool.add(new byte[]{ 8, (byte)((constantPool.size() + 2) / 256), (byte)(constantPool.size() + 2)});
		int returnValue = constantPool.size();
		addValueToConstantReferences(constantIndices, ConstantType.String, toWrite, returnValue);
		writeConstantValue(constantPool, toWrite);
		return returnValue;
	}
	// Field references have value 9
	public static void createFieldReferenceLocal(ClassFileSharedData sharedData, String nameInCode, String type) {
		createFieldReferenceConstant(sharedData.constantPool, sharedData.className, nameInCode, type, sharedData.constantIndices);
	}
	public static void createFieldReferenceNonLocal(ClassFileSharedData sharedData, String otherClassName, String nameInCode, String type) {
		createFieldReferenceConstant(sharedData.constantPool, otherClassName, nameInCode, type, sharedData.constantIndices);
	}
	public static void createFieldReferenceConstant(ClassFileSharedData sharedData, String classHierarchy, String nameInCode, String type) {
		createFieldReferenceConstant(sharedData.constantPool, classHierarchy, nameInCode, type, sharedData.constantIndices);
	}
	public static void createFieldReferenceConstant(List<byte[]> constantPool, String classHierarchy, String nameInCode,
													 String type, Map<ConstantType, Map<String, Integer>> constantIndices) {

		if (alreadyContainsReference(constantIndices, ConstantType.FieldReference, MuseUtils.fieldName(classHierarchy, nameInCode))) {
			return;
		}
		if (!constantIndices.containsKey(ConstantType.ClassName)) {
			constantIndices.put(ConstantType.ClassName, new HashMap<String, Integer>());
		}
		if (!constantIndices.get(ConstantType.ClassName).containsKey(classHierarchy)) {
			createClassNameConstant(constantPool, classHierarchy, constantIndices);
		}
		int thingToLinkIndex = constantIndices.get(ConstantType.ClassName).get(classHierarchy);
		// The + 2 indicates the index of the constant value we're about to write. The 2 is because we write 2 items
		// to the pool: the pointer and the constant value.
		constantPool.add(new byte[]{ 9, (byte)(thingToLinkIndex / 256), (byte)thingToLinkIndex,
				(byte)((constantPool.size() + 2) / 256), (byte)(constantPool.size() + 2) });
		addValueToConstantReferences(constantIndices, ConstantType.FieldReference, MuseUtils.fieldName(classHierarchy, nameInCode), constantPool.size());
		constantPool.add(new byte[]{ 12, (byte)((constantPool.size() + 2) / 256), (byte)(constantPool.size() + 2),
				(byte)((constantPool.size() + 3) / 256), (byte)(constantPool.size() + 3) });
		writeConstantValue(constantPool, nameInCode);
		writeConstantValue(constantPool, type);
	}
	public static void createGlobalVariable(ClassFileSharedData sharedData, String nameInCode, String inputAndReturnType) {
		createFieldReferenceLocal(sharedData, nameInCode, inputAndReturnType);
		writeConstantValue(sharedData, inputAndReturnType);
		writeConstantValue(sharedData, nameInCode);
	}
	// Field references have value 10
	public static void createMethodReferenceConstant(ClassFileSharedData sharedData, String nameInCode, String inputAndReturnType) {
		createMethodReferenceConstant(sharedData.constantPool, sharedData.className, nameInCode, inputAndReturnType, sharedData.constantIndices, true);
	}
	public static void createMethodReferenceConstant(ClassFileSharedData sharedData, String classHierarchy, String nameInCode,
													 String inputAndReturnType) {
		createMethodReferenceConstant(sharedData.constantPool, classHierarchy, nameInCode, inputAndReturnType, sharedData.constantIndices, true);
	}
	public static void createMethodReferenceConstant(ClassFileSharedData sharedData, String classHierarchy, String nameInCode,
													 String inputAndReturnType, boolean sameClassAsCurrent) {
		createMethodReferenceConstant(sharedData.constantPool, classHierarchy, nameInCode, inputAndReturnType, sharedData.constantIndices, sameClassAsCurrent);
	}
	public static void createMethodReferenceConstant(List<byte[]> constantPool, String classHierarchy, String nameInCode,
													  String inputAndReturnType, Map<ConstantType, Map<String, Integer>> constantIndices, boolean sameClassAsCurrent) {
		String key = classHierarchy + (sameClassAsCurrent ? "/" : ".") + nameInCode + ":" + inputAndReturnType;
		if (alreadyContainsReference(constantIndices, ConstantType.MethodReference, key)) {
			return;
		}
		if (!constantIndices.containsKey(ConstantType.ClassName)) {
			constantIndices.put(ConstantType.ClassName, new HashMap<String, Integer>());
		}
		if (!constantIndices.get(ConstantType.ClassName).containsKey(classHierarchy)) {
			createClassNameConstant(constantPool, classHierarchy, constantIndices);
		}
		int thingToLinkIndex = constantIndices.get(ConstantType.ClassName).get(classHierarchy);
		// The + 2 indicates the index of the constant value we're about to write. The 2 is because we write 2 items
		// to the pool: the pointer and the constant value.
		constantPool.add(new byte[]{ 10, (byte)(thingToLinkIndex / 256), (byte)thingToLinkIndex,
				(byte)((constantPool.size() + 2) / 256), (byte)(constantPool.size() + 2) });
		addValueToConstantReferences(constantIndices, ConstantType.MethodReference, key, constantPool.size());
		constantPool.add(new byte[]{ 12, (byte)((constantPool.size() + 2) / 256), (byte)(constantPool.size() + 2),
				(byte)((constantPool.size() + 3) / 256), (byte)(constantPool.size() + 3) });
		writeConstantValue(constantPool, nameInCode);
		writeConstantValue(constantPool, inputAndReturnType);
	}


	// Used to write strings in UTF8 (id 1) to the constant pool
	public static void writeConstantValue(List<byte[]> constantPool, String toWrite, Map<ConstantType, Map<String, Integer>> constantIndices) {
		if (alreadyContainsReference(constantIndices, ConstantType.Constant, toWrite)) {
			return;
		}
		writeConstantValue(constantPool, toWrite);
		addValueToConstantReferences(constantIndices, ConstantType.Constant, toWrite, constantPool.size());
	}
	public static void writeConstantValue(ClassFileSharedData sharedData, String toWrite) {
		if (alreadyContainsReference(sharedData.constantIndices, ConstantType.Constant, toWrite)) {
			return;
		}
		writeConstantValue(sharedData.constantPool, toWrite);
		addValueToConstantReferences(sharedData.constantIndices, ConstantType.Constant, toWrite, sharedData.constantPool.size());
	}
	public static void writeConstantValue(List<byte[]> constantPool, String toWrite) {
		byte[] stringToWrite = toWrite.getBytes();
		MuseUtils.assertNumFitsIn4Bytes(stringToWrite.length);
		constantPool.add(new byte[stringToWrite.length + 3]);
		constantPool.get(constantPool.size() - 1)[0] = 1;
		constantPool.get(constantPool.size() - 1)[1] = (byte)(stringToWrite.length / 256);
		constantPool.get(constantPool.size() - 1)[2] = (byte)stringToWrite.length;
		for (int i = 0; i < stringToWrite.length; i++) {
			constantPool.get(constantPool.size() - 1)[i + 3] = stringToWrite[i];
		}
	}


	private static void addValueToConstantReferences(Map<ConstantType, Map<String, Integer>> constantIndices, ConstantType type, String toAdd, int index) {
		if (!constantIndices.containsKey(type)) {
			constantIndices.put(type, new HashMap<String, Integer>());
		}
		Map<String, Integer> fieldReferences = constantIndices.get(type);
		if (!fieldReferences.containsKey(toAdd)) {
			fieldReferences.put(toAdd, index);
		}
	}
	public static boolean alreadyContainsReference(Map<ConstantType, Map<String, Integer>> constantIndices, ConstantType type, String toAdd) {
		return constantIndices.containsKey(type) && constantIndices.get(type).containsKey(toAdd);
	}
}
