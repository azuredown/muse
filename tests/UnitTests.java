
public class UnitTests {

	public static void main(String[] args) {

		byte[] output = MuseUtils.positiveIntToBytesFiniteSize(0, 2);
		if (output.length != 2) {
			throw new RuntimeException("Invalid output length");
		}
		if (output[0] != 0) {
			throw new RuntimeException("Invalid option.");
		}
		if (output[1] != 0) {
			throw new RuntimeException("Invalid option.");
		}

		output = MuseUtils.positiveIntToBytesFiniteSize(1, 2);
		if (output.length != 2) {
			throw new RuntimeException("Invalid output length");
		}
		if (output[0] != 0) {
			throw new RuntimeException("Invalid option.");
		}
		if (output[1] != 1) {
			throw new RuntimeException("Invalid option.");
		}

		output = MuseUtils.positiveIntToBytesFiniteSize(2, 2);
		if (output.length != 2) {
			throw new RuntimeException("Invalid output length");
		}
		if (output[0] != 0) {
			throw new RuntimeException("Invalid option.");
		}
		if (output[1] != 2) {
			throw new RuntimeException("Invalid option.");
		}

		output = MuseUtils.positiveIntToBytesFiniteSize(256, 2);
		if (output.length != 2) {
			throw new RuntimeException("Invalid output length");
		}
		if (output[0] != 1) {
			throw new RuntimeException("Invalid option.");
		}
		if (output[1] != 0) {
			throw new RuntimeException("Invalid option.");
		}

		output = MuseUtils.positiveIntToBytesFiniteSize(0, 2);
		if (output.length != 2) {
			throw new RuntimeException("Invalid output length");
		}
		if (output[0] != 0) {
			throw new RuntimeException("Invalid option.");
		}
		if (output[1] != 0) {
			throw new RuntimeException("Invalid option.");
		}

		output = MuseUtils.intToBytes(1, 2);
		if (output.length != 2) {
			throw new RuntimeException("Invalid output length");
		}
		if (output[0] != 0) {
			throw new RuntimeException("Invalid option.");
		}
		if (output[1] != 1) {
			throw new RuntimeException("Invalid option.");
		}

		output = MuseUtils.intToBytes(2, 2);
		if (output.length != 2) {
			throw new RuntimeException("Invalid output length");
		}
		if (output[0] != 0) {
			throw new RuntimeException("Invalid option.");
		}
		if (output[1] != 2) {
			throw new RuntimeException("Invalid option.");
		}

		output = MuseUtils.intToBytes(256, 2);
		if (output.length != 2) {
			throw new RuntimeException("Invalid output length");
		}
		if (output[0] != 1) {
			throw new RuntimeException("Invalid option.");
		}
		if (output[1] != 0) {
			throw new RuntimeException("Invalid option.");
		}

		boolean encounteredException = false;
		try {
			MuseUtils.intToBytes(128, 1);
		} catch (RuntimeException e) {
			encounteredException = true;
		}
		if (!encounteredException) {
			throw new RuntimeException("Should have encountered a runtime exception.");
		}

		encounteredException = false;
		try {
			MuseUtils.intToBytes(-129, 1);
		} catch (RuntimeException e) {
			encounteredException = true;
		}
		if (!encounteredException) {
			throw new RuntimeException("Should have encountered a runtime exception.");
		}
		System.out.println("Finished unit tests");
	}

}