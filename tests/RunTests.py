import os
import subprocess
import glob
import shutil


def assertOutputEqual(className, expectedOutput, arguments = ""):
	returnValue = os.system('java MuseMain -c ' + className + '.muse')
	if (returnValue != 0):
		print("Failed on class: " + className + ". Return value is non-zero (" + str(returnValue) + ")")
		print("")
		exit()
	actualOutput = subprocess.check_output('java ' + className + " " + arguments, shell = True).decode('utf-8').replace('\r\n', '\n')
	if actualOutput != expectedOutput:
		if actualOutput.rstrip() == expectedOutput.rstrip():
			print("Failed on class " + className + " due to trailing whitespace only. Expected: '" + expectedOutput + "' but got: '" + actualOutput + "'")
			print("")
			exit()
		print("Failed on class: " + className + ". Expected: '" + expectedOutput + "' but got: '" + actualOutput + "'")
		print("")
		exit()

def assertError(className):
	# I would use /dev/null but for some reason I'm getting an error saying it can't find it.
	returnValue = os.system('java MuseMain ' + className + '.muse 2>randomFile')
	if (returnValue == 0):
		print("Class " + className + " should not have completed successfully. Return value is zero.")
		print("")
		exit()

# Remove class files to make sure lingering files don't cause a test to pass when it should fail
# Must be before or after all tests or it will cause some to fail
os.system('rm *.class')

# Compile and copy all the files
returnValue = os.system('javac ../src/*.java')
if returnValue != 0:
	print("Javac returned with code " + returnValue)
	exit()
files = glob.iglob(os.path.join("../src", "*.class"))
for file in files:
	if os.path.isfile(file):
		shutil.copy2(file, ".")

# Run tests
assertOutputEqual('TestComment', '')
assertOutputEqual('TestCommentAndPrint', 'Look ma, no brackets!\nHello world\n')
assertOutputEqual('TestEmptyFile', '')
assertOutputEqual('TestEmptyPrint', '\n')
assertOutputEqual('TestMultiplePrints', 'Look ma, no brackets!\n\nHello world\n')
assertOutputEqual('TestPrintSameThingMultipleTimes', 'Look ma, no brackets!\nLook ma, no brackets!\n')
assertOutputEqual('TestPrintJavaValues', 'java/lang/Object\nmain\n([Ljava/lang/String;)V\nCode\njava/lang/System\njava/io/PrintStream\nout\nprintln\n')
assertOutputEqual('TestPrintInt', '-1\n0\n1\n2\n3\n4\n5\n-2\n-128\n6\n127\n-129\n128\n-32768\n32767\n')
assertOutputEqual('TestAssignment', '5\nTest\n13\n4\n5\n6\n20\n13\n16\n')
assertOutputEqual('TestOverwritingVariable', '1\n')
assertOutputEqual('TestIncrementDecrement', '5\n6\n4\n')
assertOutputEqual('TestOpEquals', '5\n6\n3\n2\n4\n1\n257\n-255\n256\n1\n')
assertOutputEqual('TestBools', 'true\nfalse\ntrue\nfalse\n')
assertOutputEqual('TestIf', '')
assertOutputEqual('TestMultipleIfs', '3\n')
assertOutputEqual('TestNestedIfs', '1\n')
assertOutputEqual('TestGotoConsideredHarmful', '1\n2\n3\n4\n')
assertOutputEqual('TestWhileDoWhile', 'Should still print\n')
assertOutputEqual('TestAndOrConditions', 'Should print 1\nShould print 2\nShould print 3\nShould print 4\nShould prin\
t 5\nShould print 6\nShould print 7\nShould print 8\nShould print 9\nShould print 10\nShould print 11\nShould print 12\
\nShould print 13\nShould print 14\nShould print 15\n')
assertOutputEqual('TestArithmeticBool', 'Should print 1\nShould print 2\nShould print 3\nShould print 4\nShould print 5\
\nShould print 6\nShould print 7\nShould print 8\nShould print 9\n')
assertOutputEqual('TestAssignExpression', '8\n0\n24\n6\n7\n8\n9\n10\n')
assertOutputEqual('TestArgv', '2\nHello\n2\n1\n6\n2\nHello\nworld\n', "Hello world")

# A dysfunction is a class that is only initialized for its constructor
assertOutputEqual('TestDysfunctionNoArguments', '5\n')
assertOutputEqual('TestDysfunctionWithArgument', '1\n')
assertOutputEqual('TestDysfunctionWithArguments', '1\n2\n')
assertOutputEqual('TestDysfunctions', '5\n5\n6\n6\n5\n6\nhello\n')
assertOutputEqual('TestDysfunctionWithBreak', '5\n')
assertOutputEqual('TestVariableFromDysfunction', '6\n')

assertOutputEqual('TestFizzBuzz', '1\n2\nFizz\n4\nBuzz\nFizz\n7\n8\nFizz\nBuzz\n11\nFizz\n13\n14\nFizzBuzz\n16\n17\nFi\
zz\n19\nBuzz\nFizz\n22\n23\nFizz\nBuzz\n26\nFizz\n28\n29\nFizzBuzz\n31\n32\nFizz\n34\nBuzz\nFizz\n37\n38\nFizz\nBuzz\n4\
1\nFizz\n43\n44\nFizzBuzz\n46\n47\nFizz\n49\nBuzz\nFizz\n52\n53\nFizz\nBuzz\n56\nFizz\n58\n59\nFizzBuzz\n61\n62\nFiz\
z\n64\nBuzz\nFizz\n67\n68\nFizz\nBuzz\n71\nFizz\n73\n74\nFizzBuzz\n76\n77\nFizz\n79\nBuzz\nFizz\n82\n83\nFizz\nBuzz\n8\
6\nFizz\n88\n89\nFizzBuzz\n91\n92\nFizz\n94\nBuzz\nFizz\n97\n98\nFizz\nBuzz\n')
assertOutputEqual('TestVarStartsWithNumber', '99\n')
assertOutputEqual('TestAndOrMini', '') # For a bug where we skipped the first stack map entry

# This requires a dysfunction to know about who's calling it. Perhaps implement this later.
#assertOutputEqual('TestStaticVar', '6\n')

# Disabled due to calling out to an external class which is no longer supported
#assertOutputEqual('TestCreateTestClass', 'Hello\n')

assertOutputEqual('TestInstantiationAndAssignment', 'Hello\n')
assertOutputEqual('TestWhatsAConstructorSingleVarNoPrint', '1\n')
assertOutputEqual('TestWhatsAConstructorSingleVar', '1\n1\n')
assertOutputEqual('TestWhatsAConstructorMultipleVars', '1\n2\n1\n')

# Note: File: TestInfiniteLoop not tested because I don't know how to test an infinite loop

assertError('TestRandomIndent')
assertError('TestStaticVarShouldFail')
#assertError('TestUnindentedIf')
assertError('TestDuplicateNames')

print("Finished integration tests")

# Run unit tests
os.system('cp ../src/MuseUtils.java MuseUtils.java')
os.system('javac UnitTests.java')
os.system('java UnitTests')
