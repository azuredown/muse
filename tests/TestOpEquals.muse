// For bytes
var <- 5
print var
var += 1
print var
var /= 2
print var
var -= 1
print var
var *= 2
print var

// For shorts
var <- 1
print var
var += 256
print var
var -= 512
print var
var <- 1
var *= 256
print var
var /= 256
print var